package acumen.ui.visualeditor

import java.awt.BorderLayout
import acumen.ui.App
import swing._
import javax.swing.{JPopupMenu, SwingUtilities, JPanel, JTabbedPane}
import java.util.concurrent.Callable

class VisualEditorTab extends BorderPanel {
  val editorView = new VisualEditorView

  val updateSceneAction = new Action("Update Scene") {
    def apply() { editorView.app.updateScene(App.ui.codeArea.textArea.getText) }
  }
  val updateSceneButton = new Button(updateSceneAction)
  val canvasPanel = new JPanel()

  SwingUtilities.invokeLater(new Runnable(){
    def run() {
      JPopupMenu.setDefaultLightWeightPopupEnabled(false)
      createCanvas()
      editorView.startApp()
    }
  })

  def createCanvas() {
    canvasPanel.setLayout(new BorderLayout())
    canvasPanel.add(editorView.canvas, BorderLayout.CENTER)
    canvasPanel.add(updateSceneButton.peer, BorderLayout.NORTH)
    peer.add(canvasPanel)
  }
}
