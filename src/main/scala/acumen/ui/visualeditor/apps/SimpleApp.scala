package acumen.ui.visualeditor.apps

import com.jme3.system.JmeCanvasContext
import com.jme3.app.SimpleApplication
import com.jme3.scene.shape.{Dome, Cylinder, Sphere, Box}
import com.jme3.math._
import com.jme3.scene.{Spatial, Mesh, Node, Geometry}
import com.jme3.material.Material
import com.jme3.input.controls._
import com.jme3.collision.{CollisionResult, CollisionResults}
import com.jme3.scene.control.BillboardControl
import com.jme3.font.BitmapText
import com.jme3.light.{AmbientLight, DirectionalLight}
import com.jme3.scene.debug.Grid
import scala.collection.JavaConversions._
import scala.swing._
import scala.Some
import scala.swing.Action
import acumen._
import acumen.ui.App
import acumen.ui.visualeditor.apps.ViewMode._
import acumen.ui.visualeditor.apps.ToolMode._
import acumen.Pretty.pprint
import com.jme3.bounding.BoundingBox
import java.awt.event.{InputEvent, KeyEvent}
import javax.swing.{JColorChooser, JMenu, KeyStroke}
import javax.vecmath.Vector3d
import java.util.concurrent.Callable

class SimpleApp extends SimpleApplication {
  val models = new Node()
  var grid: Geometry = null
  var axes: Node = null
  var hud: Hud = null
  var curViewMode: ViewMode = Isometric
  var curToolMode: ToolMode = Select
  val history = new CommandHistory()
  var ast: Prog = null
  var classes: Map[String, ClassDesc] = null
  var instances: List[InstanceTree] = null

  var triggers: InputTriggers = null
  var selectedNode: Node = null
  var selectedNodeBeforeTransform: Spatial = null
  var lastRmbPressMousePos = new Vector2f()
  var lastRmbReleaseMousePos = new Vector2f()
  var lastLmbPressMousePos = new Vector2f()
  var lastLmbReleaseMousePos = new Vector2f()
  var rmbPressed = false  // True when the right mouse button is pressed
  var lmbPressed = false  // True when the left mouse button is pressed
  var metaPressed = false // True when the user is holding the LMETA key
  var altPressed = false  // True when the user is holding the ALT key
  var cmdPressed = false  // True when the user is holding the LCONTROL key
  var curRadius: Float = 30f // Camera orbit radius
  var theta = 45f
  var phi = 60f

  val ObjDataKey: String = "ObjData"

  implicit def vector3dTo3f(vd: Vector3d): Vector3f = {
    new Vector3f(vd.x.toFloat, vd.y.toFloat, vd.z.toFloat)
  }

  private def setupInputs() {
    triggers = new InputTriggers(inputManager, actionListener, analogListener)
  }

  def setupCamera() {
    val curMousePos = inputManager.getCursorPosition.clone()
    rotateCamFromMousePos(curMousePos, curMousePos)
  }

  def setupScene() {
    viewPort.setBackgroundColor(Helpers.fromRGB(0xA6BEA6))

    val lightColor = Helpers.fromRGB(0xF8F7EE)
    val darkLightColor = lightColor.addLocal(ColorRGBA.DarkGray)

    // Light
    val dl = new DirectionalLight()
    dl.setDirection(new Vector3f(-.5f,-.3f,0f).normalizeLocal())
    dl.setColor(lightColor)
    rootNode.addLight(dl)

    val dl2 = new DirectionalLight()
    dl2.setDirection(new Vector3f(5f,3f,0f).normalizeLocal())
    dl2.setColor(darkLightColor)
    rootNode.addLight(dl2)

    val al = new AmbientLight
    al.setColor(ColorRGBA.White.mult(1.6f))
    rootNode.addLight(al)

    // Models
//    models.attachChild(mkBoxModel("A", Vector3f.ZERO, new Vector3f(1,1,1)))
//    models.attachChild(mkCylinderModel("B", new Vector3f(2,-2,2), new Vector3f(1,1,0)))
//    models.attachChild(mkBoxModel("C", new Vector3f(4,2,-2), new Vector3f(1,1,1)))
//    models.attachChild(mkSphereModel("S", new Vector3f(2, 2, 2), new Vector3f(1,0,0)))
    rootNode.attachChild(models)

    // Grid
    grid = mkGrid()
    rootNode.attachChild(grid)

    // Axes
    axes = mkAxes()
    rootNode.attachChild(axes)

    // Hud
    hud = new Hud(this)
  }

  override def simpleInitApp() {
    setDisplayFps(false)
    setDisplayStatView(false)

    setupInputs()
    setupScene()
    setupCamera()

    hudDisplayMouseState()
    setViewMode(Isometric)
    setToolMode(Select)
    clearSelectedNode()
  }

  def updateAll() {
    val text = pprint(ast)
    updateCodeArea(text)
    updateScene(text)
  }

  def updateCodeArea() {
    updateCodeArea(pprint(ast))
  }

  def updateCodeArea(text: String) {
    // Update the text area from the current ast
    Swing.onEDT {
      App.ui.codeArea.textArea.setText(text)
    }
  }

  def parseProgram(progText: String): Prog = {
    try {
      Parser.run(Parser.prog, progText)
    } catch {
      case e: Exception => null
    }
  }

  def visualizeInstance(inst: InstanceTree) = {
    for (idx <- 0 until inst.cls.threeDs.size) {
      val td = inst.cls.threeDs(idx)

      val resolvedPosOrigin = inst.findTDPositionOriginVars(idx)
      val transPosOrigin = Helpers.getPos(resolvedPosOrigin)
      val resolvedPosOffset = inst.findTDPositionOffsetVars(idx)
      val transPosOffset = Helpers.getPos(resolvedPosOffset)
      val resolvedSize = inst.findTDSizeVars(idx)
      val scale = Helpers.getSize(resolvedSize)
      val resolvedColor = inst.findTDColorVars(idx)
      val color = Helpers.getColor(resolvedColor)
      val pos = new Vector3f().addLocal(transPosOrigin).addLocal(transPosOffset)

      // The coordinates used up until this point are in Acumen's coordinate system.
      // We convert them to jMonkey coordinates to create the actual 3D object:
      // j.x = a.x, j.y = a.z, j.z = -a.y
      val posFixed = new Vector3f(pos.x, pos.z, -pos.y)
      val scaleFixed = new Vector3f(scale.x.toFloat, scale.z.toFloat, -scale.y.toFloat)
      val node  = getShape(td, posFixed, scaleFixed)

      node.getChild(0).asInstanceOf[Geometry].getMaterial.setColor("Ambient", color)
      node.getChild(0).asInstanceOf[Geometry].getMaterial.setColor("Diffuse", ColorRGBA.DarkGray)
      node.getChild(0).asInstanceOf[Geometry].getMaterial.setColor("Specular", ColorRGBA.LightGray)
      node.setUserData("ObjData", new ObjData(getShapeName(td), inst, idx, resolvedPosOrigin, resolvedPosOffset, resolvedSize, resolvedColor))
      models.attachChild(node)
    }
  }

  // Called in the render thread by the VisualEditorTab
  def updateScene(program: String) {
    this.enqueue(new Callable[Unit]() {
      def call() = {
        clearSelectedNode()
        models.detachAllChildren()
        ast = null
        classes = null
        instances = null

        if (program.contains("//") || program.contains("/*")) {
          Swing.onEDT {
            Dialog.showMessage(null, "The interactive editor currently can not handle models that feature comments.", "Comments forbidden in models")
          }
        } else {
          ast = parseProgram(program)
          if (ast != null && ast.defs.size != 0) {
            classes = ast.defs.map(c => c.name.x -> ClassDesc.extractClassDesc(c)).toMap
            instances = InstanceTree.extractInstances(ast.defs, classes)

            // Visualize all instances whose classes have 3D shapes
            try {
              instances.filter(_.cls.threeDs.size > 0).map(visualizeInstance)
            } catch {
              case e: Exception => {
                clearSelectedNode()
                models.detachAllChildren()
                Swing.onEDT {
                  Dialog.showMessage(null, "There was an error creating the 3D scene. Please fix any model errors.")
                }
              }
            }
          }
        }
      }
    })
  }


  def getShape(tdShape: TDShape, trans: Vector3f, size: Vector3f): Node = {
    tdShape match {
      case s: TDSphere => mkSphereModel("AnonSphere", trans, size)
      case b: TDBox => mkBoxModel("AnonBox", trans, size)
      case c: TDCylinder => mkCylinderModel("AnonCylinder", trans, size)
      case _ => throw new Exception("Unknown shape: " + tdShape)
    }
  }

  def getShapeName(tdShape: TDShape): String = {
    tdShape match {
      case s: TDSphere => "Sphere"
      case b: TDBox => "Box"
      case c: TDCylinder => "Cylinder"
      case _ => throw new Exception("Unknown shape: " + tdShape)
    }
  }

  private def mkGrid(): Geometry = {
    val g = new Grid(60, 60, 1f)
    g.setLineWidth(0.2f)
    val geom = new Geometry("Grid", g)
    val mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md")
    mat.setColor("Color", ColorRGBA.White)
    geom.setMaterial(mat)
    geom.center()
    geom.move(new Vector3f(0f, 0f, 0))
    geom
  }

  def mkAxes(): Node = {
    val node = new Node()

//    val x_arrow = new Arrow(new Vector3f(10f, 0f, 0f))
//    val y_arrow = new Arrow(new Vector3f(0f, 10f, 0f))
//    val z_arrow = new Arrow(new Vector3f(0f, 0f, 10f))

    //    node.attachChild(mkArrow(x_arrow, "X", new Vector3f(10f, 0f, 0f), ColorRGBA.Red))
    //    node.attachChild(mkArrow(y_arrow, "Y", new Vector3f(0f, 10f, 0f), ColorRGBA.Green))
    //    node.attachChild(mkArrow(z_arrow, "Z", new Vector3f(0f, 0f, 10f), ColorRGBA.Blue))


    val x_color = ColorRGBA.Red
    val y_color = ColorRGBA.Green
    val z_color = ColorRGBA.Blue

    val x_mat = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md") {
      setBoolean("UseMaterialColors",true)
      setColor("Ambient", ColorRGBA.Red)
      setColor("Diffuse", ColorRGBA.LightGray)
      setColor("Specular", ColorRGBA.Gray)
      setFloat("Shininess", 0f)
    }
    val y_mat = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md") {
      setBoolean("UseMaterialColors",true)
      setColor("Ambient", ColorRGBA.Green)
      setColor("Diffuse", ColorRGBA.LightGray)
      setColor("Specular", ColorRGBA.Gray)
      setFloat("Shininess", 0f)
    }
    val z_mat = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md") {
      setBoolean("UseMaterialColors",true)
      setColor("Ambient", ColorRGBA.Blue)
      setColor("Diffuse", ColorRGBA.LightGray)
      setColor("Specular", ColorRGBA.Gray)
      setFloat("Shininess", 0f)
    }

    val x_cyl_geom = new Geometry("Cylinder (x)", new Cylinder(32, 32, 0.08f, 20f, true)) { setMaterial(x_mat) }
    val y_cyl_geom = new Geometry("Cylinder (y)", new Cylinder(32, 32, 0.08f, 20f, true)) { setMaterial(y_mat) }
    val z_cyl_geom = new Geometry("Cylinder (z)", new Cylinder(32, 32, 0.08f, 20f, true)) { setMaterial(z_mat) }

    val x_cone_geom = new Geometry("Cone (x)", new Dome(Vector3f.ZERO, 2, 32, 0.3f, false)) { setMaterial(x_mat); setLocalScale(1, 5, 1) }
    val y_cone_geom = new Geometry("Cone (y)", new Dome(Vector3f.ZERO, 2, 32, 0.3f, false)) { setMaterial(y_mat); setLocalScale(1, 5, 1)  }
    val z_cone_geom = new Geometry("Cone (z)", new Dome(Vector3f.ZERO, 2, 32, 0.3f, false)) { setMaterial(z_mat); setLocalScale(1, 5, 1)  }

    val x_text = mkText("X", x_color)
    val y_text = mkText("Y", y_color)
    val z_text = mkText("Z", z_color)

    // Acumen uses a right hand coordinate system with Z pointing up
    node.attachChild(mkLabeledArrow(x_cyl_geom, x_cone_geom, x_text, new Vector3f(10f, 0f, 0f), new Vector3f(0.0001f, -1f, 0f)))
    node.attachChild(mkLabeledArrow(y_cyl_geom, y_cone_geom, y_text, new Vector3f(0f, 0f, 10f), new Vector3f(0f, -1f, 0f)))
    node.attachChild(mkLabeledArrow(z_cyl_geom, z_cone_geom, z_text, new Vector3f(0f, 10f, 0f), new Vector3f(1f, 0f, 0f)))

    node
  }

  def mkLabeledArrow(cyl: Geometry, cone: Geometry, text: Node, displace: Vector3f, orient: Vector3f): Node = {
    val node = new Node()

    cyl.lookAt(displace, Vector3f.UNIT_Y)
    cone.lookAt(orient, Vector3f.UNIT_Y)

    cone.move(displace)
    text.move(displace.mult(1.15f))

    node.attachChild(text)
    node.attachChild(cyl)
    node.attachChild(cone)

    node
  }

  def mkArrow(m: Mesh, s: String, displace: Vector3f, c: ColorRGBA): Node = {
    val node = new Node()
    val geom = new Geometry("x_arrow", m)
    val mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md")
    mat.setColor("Color", c)
    geom.setMaterial(mat)
    geom.setLocalTranslation(Vector3f.ZERO)

    val text = mkText(s, c)
    text.move(displace)
    node.attachChild(text)
    node.attachChild(geom)

    node
  }

  def mkText(s: String, c: ColorRGBA): Node = {
    val billboard = new BillboardControl()
    val guiFont = assetManager.loadFont("Interface/Fonts/Default.fnt")
    val ch = new BitmapText(guiFont, false)
    ch.setSize(.5f)
    ch.setText(s)
    ch.setColor(c)
    ch.addControl(billboard)
    ch
  }

  // A box model consists of a Node with two children.
  // Child0 is the model geometry, and Child1 is a selectionbox.
  private def mkBoxModel(name: String, trans: Vector3f, size: Vector3f): Node = {
    val box = mkBox(name, size)
    val node = new Node("ModelRootNode (" + name + ")")
    node.setLocalTranslation(trans)
    node.attachChildAt(box, 0)
    node
  }

  private def mkBox(name: String, size: Vector3f): Geometry = {
    val b = new Box(size.x, size.y, size.z)
    val geom = new Geometry("Box (" + name + ")", b)
    val mat = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md")
    mat.setBoolean("UseMaterialColors",true)
    mat.setColor("Ambient", ColorRGBA.DarkGray)
    mat.setColor("Diffuse", ColorRGBA.LightGray)
    mat.setColor("Specular", ColorRGBA.Gray)
    geom.setMaterial(mat)
    geom
  }

  private def mkSelectionBox(name: String, s: Spatial): Geometry = {
    val bb = s.getWorldBound.asInstanceOf[BoundingBox]
    val xBound = bb.getXExtent
    val yBound = bb.getYExtent
    val zBound = bb.getZExtent
    val b = new Box(1f, 1f, 1f)
    b.setMode(Mesh.Mode.Points)
    b.setPointSize(5f)
    val geom = new Geometry("SelectionBox (" + name + ")", b)
    val mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md")
    mat.setColor("Color", ColorRGBA.Red)
    geom.setMaterial(mat)
    geom.scale(xBound, yBound, zBound) // Resize the box relative the the geometry it encapsulates
    geom
  }

  private def mkSphereModel(name: String, trans: Vector3f, size: Vector3f): Node = {
    val sphere = mkSphere(name, size)
    val node = new Node("ModelRootNode (" + name + ")")
    node.setLocalTranslation(trans)
    node.attachChildAt(sphere, 0)
    node
  }

  private def mkSphere(name: String, size: Vector3f): Geometry = {
    val s = new Sphere(32, 32, size.x)
    val geom = new Geometry("Sphere (" + name + ")", s)
    val mat = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md")
    mat.setBoolean("UseMaterialColors",true)
    mat.setColor("Ambient", ColorRGBA.DarkGray)
    mat.setColor("Diffuse", ColorRGBA.LightGray)
    mat.setColor("Specular", ColorRGBA.Gray)
    geom.setMaterial(mat)
    geom
  }

  private def mkCylinderModel(name: String, trans: Vector3f, size: Vector3f): Node = {
    val cyl = mkCylinder(name, size)
    val node = new Node("ModelRootNode (" + name + ")")
    node.setLocalTranslation(trans)
    node.attachChildAt(cyl, 0)
    node
  }

  def mkCylinder(name: String, size: Vector3f): Geometry = {
    val c = new Cylinder(32, 32, size.x, size.y, true)
    val geom = new Geometry("Cylinder (" + name + ")", c)
    val mat = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md")
    mat.setBoolean("UseMaterialColors",true)
    mat.setColor("Ambient", ColorRGBA.DarkGray)
    mat.setColor("Diffuse", ColorRGBA.LightGray)
    mat.setColor("Specular", ColorRGBA.Gray)
    geom.setMaterial(mat)
    geom
  }

  def updatePos() = {
    val newLoc = selectedNode.getLocalTranslation.clone()
    val oldLoc = selectedNodeBeforeTransform.getLocalTranslation.clone()
    // Don't record the command if there was no change
    if (newLoc != oldLoc) {
      val od = selectedNode.getUserData[ObjData](ObjDataKey)
      od.updatePos(newLoc.x, newLoc.y, newLoc.z)
      updateAll()
      history.add(new MoveCommand(this, selectedNode, newLoc, oldLoc))
    }
  }

  def updateRotation() {
    val newRot = selectedNode.getLocalRotation.clone()
    val oldRot = selectedNodeBeforeTransform.getLocalRotation.clone()
    // Don't record the command if there was no change
    if (newRot != oldRot)
      history.add(new RotateCommand(this, selectedNode, newRot, oldRot))
  }

  // jMonkey represents the size as a scale vector that's applied to the underlying mesh.
  // It needs to be converted to an appropriate acumen size vector/value.
  def vecToSize(shape: TDShape, v: Vector3f): List[Float] = {
    shape match {
      case TDBox(p,s,c) => {
        val box = selectedNode.getChild(0).asInstanceOf[Geometry].getMesh.asInstanceOf[Box]
        List(v.x * box.xExtent, v.y * box.yExtent, v.z * box.zExtent)
      }
      case TDCylinder(p,s,c) => {
        val cyl = selectedNode.getChild(0).asInstanceOf[Geometry].getMesh.asInstanceOf[Cylinder]
        List(cyl.getRadius * selectedNode.getLocalScale.x, cyl.getHeight * selectedNode.getLocalScale.x)
      }
      case TDSphere(p,s,c) => {
        val sphere = selectedNode.getChild(0).asInstanceOf[Geometry].getMesh.asInstanceOf[Sphere]
        // The world scale is uniform for the sphere, as it always scales in all directions equally
        List(sphere.getRadius * selectedNode.getLocalScale.x)
      }
      case _ => throw new Exception("Unknown shape: " + shape)
    }
  }

  def updateSize() = {
    val newScale = selectedNode.getLocalScale.clone()
    val oldScale = selectedNodeBeforeTransform.getLocalScale.clone()
    // Don't record the command if there was no change
    if (newScale != oldScale) {
      val od = selectedNode.getUserData[ObjData](ObjDataKey)
      // Depending on the shape, the size can be a single value, or a vector with 2 or 3 values
      // - Box: vector with 3 numbers (x,y,z)
      // - Cylinder: vector with 2 numbers (radius, height)
      // - Cone: vector with 2 numbers (radius, height)
      // - Sphere: 1 number (radius)
      // - Text: 1 number
      od.updateSize(vecToSize(od.inst.cls.threeDs(od.idx), newScale))
      updateAll()
      history.add(new ScaleCommand(this, selectedNode, newScale, oldScale))
    }
  }

  def updateHistory() {
    if (selectedNode != null && selectedNodeBeforeTransform != null) {
      curToolMode match {
        case ToolMode.Move => updatePos()
        case ToolMode.Rotate => updateRotation()
        case ToolMode.Scale => updateSize()
        case _ =>
      }
    }
  }

  def recordSelectedNodeInfo() {
    if (selectedNode == null) return
    selectedNodeBeforeTransform = selectedNode.clone()
  }

  /////////////////////////////////////////
  //////////// Input handling /////////////
  /////////////////////////////////////////

  val analogListener = new AnalogListener {

    def onAnalog(name: String, value: Float, tpf: Float) {
      if (lmbPressed) {
        curToolMode match {
          case ToolMode.Pan   => handleCameraPan(name, value, tpf)
          case ToolMode.Orbit => handleCameraRotation(name, value, tpf)
          case ToolMode.Zoom  => handleCameraZoom(name, value, tpf)
          case ToolMode.Move   |
               ToolMode.Rotate |
               ToolMode.Scale => handleObjectTransform(name, value, tpf)
          case _ =>
        }
      }
    }

    // Logic executed in select mode when the rmb is held down and the mouse moved.
    def handleCameraRotation(name: String, value: Float, tpf: Float) {
      val curMousePos = inputManager.getCursorPosition.clone()

      name match {
        case InputTriggers.T_AXIS_X_POS
           | InputTriggers.T_AXIS_X_NEG
           | InputTriggers.T_AXIS_Y_POS
           | InputTriggers.T_AXIS_Y_NEG =>
        {
          rotateCamFromMousePos(lastLmbPressMousePos, curMousePos)
          lastLmbPressMousePos = curMousePos
        }
        case _ =>
      }
    }

    def handleCameraPan(name: String, value: Float, tpf: Float) {
      val scale = 20f
      val camPos = cam.getLocation

      curViewMode match {
        // Isometric view pans on a plane perpendicular to the camera lookAt direction.
        case ViewMode.Isometric => {
          name match {
            case InputTriggers.T_AXIS_X_POS => camPos.addLocal(0f, 0f, value*scale)
            case InputTriggers.T_AXIS_X_NEG => camPos.addLocal(0f, 0f, -value*scale)
            case InputTriggers.T_AXIS_Y_POS => camPos.addLocal(value*scale, 0f, 0f)
            case InputTriggers.T_AXIS_Y_NEG => camPos.addLocal(-value*scale, 0f, 0f)
            case _ =>
          }
          cam.setLocation(camPos)
        }
        // Looking from the top, panning along X and Y is translated into panning along X and Z axis
        case ViewMode.Top => {
          name match {
            case InputTriggers.T_AXIS_X_POS => camPos.addLocal(-value*scale, 0f, 0f)
            case InputTriggers.T_AXIS_X_NEG => camPos.addLocal(value*scale, 0f, 0f)
            case InputTriggers.T_AXIS_Y_POS => camPos.addLocal(0f, 0f, value*scale)
            case InputTriggers.T_AXIS_Y_NEG => camPos.addLocal(0f, 0f, -value*scale)
            case _ =>
          }
          cam.setLocation(camPos)
        }
        // Looking from the front, panning along X and Y is translated into panning along X and Y axis
        case ViewMode.Front => {
          name match {
            case InputTriggers.T_AXIS_X_POS => camPos.addLocal(0f, 0f, -value*scale)
            case InputTriggers.T_AXIS_X_NEG => camPos.addLocal(0f, 0f, value*scale)
            case InputTriggers.T_AXIS_Y_POS => camPos.addLocal(0f, value*scale, 0f)
            case InputTriggers.T_AXIS_Y_NEG => camPos.addLocal(0f, -value*scale, 0f)
            case _ =>
          }
          cam.setLocation(camPos)
        }
        // Looking from the left, panning along X and Y is translated into panning along Z and Y axis
        case ViewMode.Left => {
          name match {
            case InputTriggers.T_AXIS_X_POS => camPos.addLocal(value*scale, 0f, 0f)
            case InputTriggers.T_AXIS_X_NEG => camPos.addLocal(-value*scale, 0f, 0f)
            case InputTriggers.T_AXIS_Y_POS => camPos.addLocal(0f, value*scale, 0f)
            case InputTriggers.T_AXIS_Y_NEG => camPos.addLocal(0f, -value*scale, 0f)
            case _ =>
          }
          cam.setLocation(camPos)
        }
        // Looking from the left, panning along X and Y is translated into panning along -Z and Y axis
        case ViewMode.Right => {
          name match {
            case InputTriggers.T_AXIS_X_POS => camPos.addLocal(-value*scale, 0f, 0f)
            case InputTriggers.T_AXIS_X_NEG => camPos.addLocal(value*scale, 0f, 0f)
            case InputTriggers.T_AXIS_Y_POS => camPos.addLocal(0f, value*scale, 0f)
            case InputTriggers.T_AXIS_Y_NEG => camPos.addLocal(0f, -value*scale, 0f)
            case _ =>
          }
          cam.setLocation(camPos)
        }
        // Looking from the left, panning along X and Y is translated into panning along -X and Y axis
        case ViewMode.Back => {
          name match {
            case InputTriggers.T_AXIS_X_POS => camPos.addLocal(0f, 0f, value*scale)
            case InputTriggers.T_AXIS_X_NEG => camPos.addLocal(0f, 0f, -value*scale)
            case InputTriggers.T_AXIS_Y_POS => camPos.addLocal(0f, value*scale, 0f)
            case InputTriggers.T_AXIS_Y_NEG => camPos.addLocal(0f, -value*scale, 0f)
            case _ =>
          }
          cam.setLocation(camPos)
        }
      }
    }

    def handleCameraZoom(name: String, value: Float, tpf: Float) {
      val curMousePos = inputManager.getCursorPosition.clone()
      val scale: Float = 20f
      var delta: Float = 0f

      // We're only interested in changes of the Y axis
      name match {
        case InputTriggers.T_AXIS_Y_POS => delta = tpf*scale
        case InputTriggers.T_AXIS_Y_NEG => delta = -tpf*scale
        case _ => return
      }

      curRadius += delta

      curViewMode match {
        // In isometric mode we move the camera along a spherical surface.
        // We do this by modifying the curRadius, but setting the current and last
        // mouse coords to the same value. This way the only change is in the
        // radius of the sphere, but not in theta and phi.
        case ViewMode.Isometric => rotateCamFromMousePos(curMousePos, curMousePos)
        // In all other view modes we move the camera by adding or subtracting
        // from the axis that currently represents depth.
        // When viewing from the top, depth is Y axis towards us
        case ViewMode.Top => moveCameraPreserveAngles(new Vector3f(0f, delta, 0f))
        // When viewing from the top, depth is Z axis towards us
        case ViewMode.Front => moveCameraPreserveAngles(new Vector3f(0f, 0f, delta))
        // When viewing from the top, depth is X axis away from us
        case ViewMode.Left => moveCameraPreserveAngles(new Vector3f(-delta, 0f, 0f))
        // When viewing from the top, depth is X axis towards us
        case ViewMode.Right => moveCameraPreserveAngles(new Vector3f(delta, 0f, 0f))
        // When viewing from the top, depth is Z axis away from us
        case ViewMode.Back => moveCameraPreserveAngles(new Vector3f(0f, 0f, -delta))
      }
    }

    // Logic executed when move/rotate/scale tools are used. This happens
    // when the selection tool was used to pick a geometry and then another
    // tool to perform a transformation.
    def handleObjectTransform(name: String, value: Float, tpf: Float) {
      if (selectedNode != null) {
        curToolMode match {
          case ToolMode.Move => handleTransformMove(name, value, tpf)
          case ToolMode.Rotate => handleTransformRotate(name, value, tpf)
          case ToolMode.Scale => handleTransformScale(name, value, tpf)
        }
      }
    }

    // Movement depends on the ViewMode and the trigger direction
    def handleTransformMove(name: String, value: Float, tpf: Float) {
      val scale = 75f
      val look = new Vector3f()
      val left = new Vector3f()

      val n = 1
      val m = 1

      look.set(cam.getUp.normalize).negateLocal()
      left.set(cam.getLeft.normalize).negateLocal()

      val vec = name match {
        case InputTriggers.T_AXIS_X_POS => left.mult(value*scale)
        case InputTriggers.T_AXIS_X_NEG => left.mult(-value*scale)
        case InputTriggers.T_AXIS_Y_POS => look.mult(-value*scale)
        case InputTriggers.T_AXIS_Y_NEG => look.mult(value*scale)
        // Ignore mouse scroll while transforming
        case InputTriggers.T_AXIS_Z_POS |
             InputTriggers.T_AXIS_Z_NEG => return
      }

      val trans = selectedNode.getLocalTranslation.add(vec)
      roundVec(trans, n, m)

      selectedNode.setLocalTranslation(trans)
    }

    def handleTransformRotate(name: String, value: Float, tpf: Float) {
      var q: Quaternion = null
      val scale = 10f

      // Ignore mouse scroll while transforming
      name match {
        case InputTriggers.T_AXIS_Z_POS
           | InputTriggers.T_AXIS_Z_NEG => return
        case _ =>
      }

      // XXX FIXME
      q = name match {
        case InputTriggers.T_AXIS_X_POS => new Quaternion().fromAngles(0f, 0f, value*scale)
        case InputTriggers.T_AXIS_X_NEG => new Quaternion().fromAngles(0f, 0f, -value*scale)
        case InputTriggers.T_AXIS_Y_POS => new Quaternion().fromAngles(value*scale, 0f, 0f)
        case InputTriggers.T_AXIS_Y_NEG => new Quaternion().fromAngles(-value*scale, 0f, 0f)
      }
      selectedNode.setLocalRotation(selectedNode.getLocalRotation.mult(q))
    }

    def handleTransformScale(name: String, value: Float, tpf: Float) {
      val grow = 1.05f
      val shrink = 0.95f
      var scale = 1f

      // Ignore mouse scroll while transforming
      name match {
        case InputTriggers.T_AXIS_Z_POS
           | InputTriggers.T_AXIS_Z_NEG => return
        case _ =>
      }

      name match {
        case InputTriggers.T_AXIS_Y_POS => scale = grow
        case InputTriggers.T_AXIS_Y_NEG => {
          // Do not allow scaling below a threshold
          if (selectedNode.getLocalScale.length() > 0.2f)
            scale = shrink
        }
        case _ =>
      }

      selectedNode.setLocalScale(selectedNode.getLocalScale.mult(scale))
    }

    // Rounds down to the nearest multiple of the fraction n/m:
    // Ex: 2.7 with n=1 m=4 rounds to 2.5.
    // If n == m, no rounding is performed
    def roundVec(v: Vector3f, n: Int, m: Int) {
      if (n == m) return

      val r = n.toFloat/m.toFloat
      v.setX((v.x / r).toInt * r)
      v.setY((v.y / r).toInt * r)
      v.setZ((v.z / r).toInt * r)
    }
  }

  def deleteSelectedNode() {
    val objData = selectedNode.getUserData[ObjData](ObjDataKey)

    // Find the class that has this shape in its _3D
    val idx = objData.idx
    val className = objData.inst.cls.name
    val classDef = ast.defs.find(_.name.x == className).get
    val tdSecRhs = classDef.priv.find(s => s.x.x == "_3D").get.rhs
    val vecElems = tdSecRhs.asInstanceOf[ExprRhs].e.asInstanceOf[ExprVector]

    // If the first element is not a vector, then this _3D has a single object in it.
    if (!vecElems.l(0).isInstanceOf[ExprVector] && idx == 0) {
      // Remove the whole _3D statement
      classDef.priv = classDef.priv.filter(s => s.x.x != "_3D")
    } else {
      // Create a new ExprVector but skip the shape we want to delete
      val (start, _ :: end) = vecElems.l.splitAt(idx)
      vecElems.l = start ::: end
    }

    // Recreate the text and scene
    updateAll()
  }

  def deleteSelectedNodeParent() {
    val objData = selectedNode.getUserData[ObjData](ObjDataKey)
    objData

    objData.inst.parent match {
      case Some(pInst) => {
        val classDef = ast.defs.find(_.name.x == pInst.cls.name).get
        classDef.priv = classDef.priv.filter(s => s.x.x != objData.inst.name)
        updateAll()
      }
      case _ =>
    }
  }

  def deleteSelectedNodeOrParent() {
    val objData = selectedNode.getUserData[ObjData](ObjDataKey)
    val shapeName = objData.name
    val creatorName = objData.inst.cls.name
    val parName = objData.inst.parent match { case Some(p) => p.cls.name; case _ => "Main" }
    val deleteWhat = new DeleteNodeDialog(shapeName, creatorName, parName).deleteWhat
    deleteWhat match {
      case Some(x) => {
        x match {
          case DeleteNode() => deleteSelectedNode()
          case DeleteParent() => deleteSelectedNodeParent()
          case _ =>
        }
      }
      case _ =>
    }
  }

  def changeColorSelectedNode() {
    val material = selectedNode.getChild(0).asInstanceOf[Geometry].getMaterial
    val curColor = material.getParam("Ambient").getValue.asInstanceOf[ColorRGBA]
    val swingColor = new Color(curColor.r, curColor.g, curColor.b, curColor.a)
    val c = JColorChooser.showDialog(null, "Select Color", swingColor)
    if(c != null) {
      val r = c.getRed/255f
      val g = c.getGreen/255f
      val b = c.getBlue/255f
      val newColor = new ColorRGBA(r, g, b, 0f)
      val od = selectedNode.getUserData[ObjData](ObjDataKey)
      material.setColor("Ambient", newColor)
      od.updateColor(r, g, b)
      updateAll()
    }
  }

  // Create a _3D shape given the shape name. It's position at 0.
  def shapeToAstTD(shape: String): ExprVector = {
    val shapeExpr = shape match {
      case "Box" => List(Lit(GStr("Box")),
                         ExprVector(List(Lit(GInt(0)),Lit(GInt(0)),Lit(GInt(0)))), // Pos
                         ExprVector(List(Lit(GInt(1)),Lit(GInt(1)),Lit(GInt(1)))), // Size
                         ExprVector(List(Lit(GDouble(0.3)),Lit(GDouble(0.7)),Lit(GDouble(0.2)))), // Color
                         ExprVector(List(Lit(GInt(0)),Lit(GInt(0)),Lit(GInt(0)))))// Rot
      case "Sphere" => List(Lit(GStr("Sphere")),
                            ExprVector(List(Lit(GInt(0)),Lit(GInt(0)),Lit(GInt(0)))), // Pos
                            Lit(GInt(1)), // Size
                            ExprVector(List(Lit(GDouble(0.3)),Lit(GDouble(0.7)),Lit(GDouble(0.2)))), // Color
                            ExprVector(List(Lit(GInt(0)),Lit(GInt(0)),Lit(GInt(0)))))// Rot
      case "Cylinder" => List(Lit(GStr("Cylinder")),
                              ExprVector(List(Lit(GInt(0)),Lit(GInt(0)),Lit(GInt(0)))), // Pos
                              ExprVector(List(Lit(GInt(1)),Lit(GInt(5)))), // Size
                              ExprVector(List(Lit(GDouble(0.3)),Lit(GDouble(0.7)),Lit(GDouble(0.2)))), // Color
                              ExprVector(List(Lit(GInt(0)),Lit(GInt(0)),Lit(GInt(0)))))// Rot

      case _ => throw new Exception("Unknown shape")
    }
    ExprVector(shapeExpr)
  }

  def addPrimitiveToClass(shape: String, className: String) {
    val astShape = shapeToAstTD(shape)

    // Make sure the parent has a _3D section.
    val classDef = ast.defs.find(_.name.x == className).get
    classDef.priv.find(s => s.x.x == "_3D") match {
      // If it doesn't have, add one.
//      case None => classDef.priv = classDef.priv :+ Init(Name("_3D", 0), ExprRhs(ExprVector(List())))
      case None => classDef.priv = classDef.priv :+ Init(Name("_3D", 0), ExprRhs(astShape))
      case Some(init) =>
        val tdRhs = init.rhs.asInstanceOf[ExprRhs]
        val vecElems = tdRhs.e.asInstanceOf[ExprVector]

        // If _3D exists, but is a single element, convert it to a vector
        if (!vecElems.l(0).isInstanceOf[ExprVector]) {
          init.rhs = ExprRhs(ExprVector(List(vecElems, astShape)))
        } else {
          // Else, just add the new shape to the list
          vecElems.l = vecElems.l :+ astShape
        }
    }
    updateAll()
  }

  def addPrimitive(shape: String) {
    val availClasses = classes.values.map(_.name).toList.sorted
    val whichClass = new AddDialog(shape, availClasses).whichClass
    whichClass match {
      case Some(cls) => addPrimitiveToClass(shape, cls)
      case _ =>
    }
  }

  // Tries to build a param + suffix combo that doesn't exist already
  def genUniqueVar(existing: List[String], param: String): String = {
    val suffixes = ('0' to '9') ++ ('a' to 'z') ++ ('A' to 'Z')
    param + suffixes.find(s => !existing.contains(param + s)).get
  }

  // @existing already assigned variables
  // @params the names of the parameter for the class constructor
  // @i the number of vars to generate
  def genUniqueVars(existing: List[String], params: List[String]): List[String] = {
    params.map(p => genUniqueVar(existing, p)).toList
  }

  // Generate a "create" for the given class.
  def classToAstInstance(instCls: String, parClass: String): Init = {
    val instClassDesc = classes.values.find(_.name == instCls).get
    val parClassDesc = classes.values.find(_.name == parClass).get
    val existingVarNames = parClassDesc.locals.map(_.name.x).toList ++ parClassDesc.insts
    val paramName = instCls.head.toLower.toString
    // var start with the same letter as the lowercase class
    val varName = genUniqueVars(existingVarNames,List(paramName)).head
    // args must differ from locals plus the new var name
    val args = genUniqueVars(instClassDesc.locals.map(_.name.x).toList :+ varName, instClassDesc.args.toList)
    Init(Name(varName, 0), NewRhs(ClassName(instCls), args.map(a => Var(Name(a, 0)))))
  }

  // @instClass is the class we're instantiating
  // @cls is the class which will get the create statement
  def addInstanceToClass(instClass: String, cls: String) {
    val classDef = ast.defs.find(_.name.x == cls).get
    val astInst = classToAstInstance(instClass, cls)
    classDef.priv = classDef.priv :+ astInst
    updateAll()
  }

  def addInstance(instClass: String) {
    val availClasses = classes.values.map(_.name).toList.sorted
    val whichClass = new AddDialog(instClass, availClasses).whichClass
    whichClass match {
      case Some(cls) => addInstanceToClass(instClass, cls)
      case _ =>
    }
  }

  // http://jmonkeyengine.org/wiki/doku.php/jme3:advanced:mouse_picking?s=mouse&s=cursor
  val actionListener = new ActionListener {

    def popUpMenu() {
      Swing.onEDT {
        val popup = new javax.swing.JPopupMenu()

        val selectItem = new MenuItem(new Action("Select") {
          mnemonic = KeyEvent.VK_E
          accelerator = Some(KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.ALT_DOWN_MASK))
          toolTip = "Select an object"
          def apply() { setToolMode(ToolMode.Select) }
        })
        val panItem = new MenuItem(new Action("Pan") {
          mnemonic = KeyEvent.VK_A
          accelerator = Some(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.ALT_DOWN_MASK))
          toolTip = "Pan scene"
          def apply() { setToolMode(ToolMode.Pan) }
        })
        val orbitItem = new MenuItem(new Action("Orbit") {
          mnemonic = KeyEvent.VK_O
          accelerator = Some(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.ALT_DOWN_MASK))
          toolTip = "Orbit scene"
          def apply() { setToolMode(ToolMode.Orbit) }
        })
        val zoomItem = new MenuItem(new Action("Zoom") {
          mnemonic = KeyEvent.VK_Z
          accelerator = Some(KeyStroke.getKeyStroke(KeyEvent.VK_Z, InputEvent.ALT_DOWN_MASK))
          toolTip = "Zoom scene"
          def apply() { setToolMode(ToolMode.Zoom) }
        })

        val rotateItem = new MenuItem(new Action("Rotate") {
          mnemonic = KeyEvent.VK_R
          accelerator = Some(KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.ALT_DOWN_MASK))
          toolTip = "Rotate selected object"
          def apply() { setToolMode(ToolMode.Rotate) }
        })
        val scaleItem = new MenuItem(new Action("Scale") {
          mnemonic = KeyEvent.VK_C
          accelerator = Some(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.ALT_DOWN_MASK))
          toolTip = "Scale selected object"
          def apply() { setToolMode(ToolMode.Scale) }
        })
        val moveItem = new MenuItem(new Action("Move") {
          mnemonic = KeyEvent.VK_M
          accelerator = Some(KeyStroke.getKeyStroke(KeyEvent.VK_M, InputEvent.ALT_DOWN_MASK))
          toolTip = "Move selected object"
          def apply() { setToolMode(ToolMode.Move) }
        })

        val deleteItem = new MenuItem(new Action("Delete"){
          mnemonic = KeyEvent.VK_D
          accelerator = Some(KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.ALT_DOWN_MASK))
          toolTip = "Delete selected or parent object"
          def apply() { deleteSelectedNodeOrParent() }
        })

        val changeColorItem = new MenuItem(new Action("Change Color") {
          mnemonic = KeyEvent.VK_K
          accelerator = Some(KeyStroke.getKeyStroke(KeyEvent.VK_K, InputEvent.ALT_DOWN_MASK))
          toolTip = "Change color of selected object"
          def apply() { changeColorSelectedNode() }
        })

        val addPrimitiveMenu = new JMenu("Add Primitive")
        val addInstanceMenu = new JMenu("Add Instance")

        List("Box", "Sphere", "Cylinder").foreach(shape =>
          addPrimitiveMenu.add(
            new MenuItem(new Action(shape){
              toolTip = "Add a " + shape
              def apply() { addPrimitive(shape) }
            }).peer
        ))

        if (classes != null) {
          val availClasses = classes.values.filter(c => c.name != "Main" && c.threeDs.size > 0).map(_.name)
          availClasses.foreach(cls =>
            addInstanceMenu.add(
              new MenuItem(new Action(cls){
                toolTip = "Class " + cls
                def apply() { addInstance(cls) }
              }).peer
            ))
        }

        val mousePt = new Point(lastRmbReleaseMousePos.x.toInt, lastRmbPressMousePos.y.toInt)
        val canvas = getContext.asInstanceOf[JmeCanvasContext].getCanvas

        val labelText = if (selectedNode != null) " Node: " + selectedNode.getChild(0).toString else " Tools"
        popup.add("Label", new Label(labelText).peer)

        popup.addSeparator()

        if (selectedNode == null) {
          rotateItem.enabled = false
          scaleItem.enabled = false
          moveItem.enabled = false
          deleteItem.enabled = false
          changeColorItem.enabled = false
        }

        popup.add(rotateItem.peer)
        popup.add(scaleItem.peer)
        popup.add(moveItem.peer)
        popup.add(changeColorItem.peer)

        popup.addSeparator()

        popup.add(addPrimitiveMenu)
        popup.add(addInstanceMenu)

        popup.addSeparator()

        popup.add(deleteItem.peer)

        popup.addSeparator()

        popup.add(selectItem.peer)
        popup.add(panItem.peer)
        popup.add(orbitItem.peer)
        popup.add(zoomItem.peer)

        // In case there isn't a valid AST, just disable all actions
        if (ast == null)
          popup.getComponents.map(_.setEnabled(false))

        popup.show(canvas, mousePt.x, canvas.getHeight-mousePt.y)
      }
    }

    def onAction(name: String, keyPressed: Boolean, tpf: Float) {
      name match {
        case InputTriggers.T_RMB if keyPressed =>
        {
          rmbPressed = true
          lastRmbPressMousePos = inputManager.getCursorPosition.clone()
        }
        case InputTriggers.T_RMB if !keyPressed =>
        {
          rmbPressed = false
          lastRmbReleaseMousePos = inputManager.getCursorPosition.clone()
          // Handle menu creation
          popUpMenu()
        }
        case InputTriggers.T_LMB if keyPressed =>
        {
          lmbPressed = true
          lastLmbPressMousePos = inputManager.getCursorPosition.clone()
          recordSelectedNodeInfo()
        }
        case InputTriggers.T_LMB if !keyPressed =>
        {
          lmbPressed = false
          lastLmbReleaseMousePos = inputManager.getCursorPosition.clone()
          if (curToolMode == Select)
            handlePicking()
          updateHistory()
        }
        case InputTriggers.T_META_KEY =>
        {
          metaPressed = keyPressed
        }
        case InputTriggers.T_ALT_KEY =>
        {
          altPressed = keyPressed
        }
        case InputTriggers.T_CMD_KEY =>
        {
          cmdPressed = keyPressed
        }
        case InputTriggers.T_MOVE_KEY if altPressed && !keyPressed=>
        {
          if (selectedNode != null) setToolMode(ToolMode.Move)
        }
        case InputTriggers.T_ROTATE_KEY if altPressed && !keyPressed =>
        {
          if (selectedNode != null) setToolMode(ToolMode.Rotate)
        }
        case InputTriggers.T_SCALE_KEY if altPressed && !keyPressed =>
        {
          if (selectedNode != null) setToolMode(ToolMode.Scale)
        }
        case InputTriggers.T_SELECT_KEY if altPressed && !keyPressed =>
        {
          setToolMode(ToolMode.Select)
        }
        case InputTriggers.T_PAN_KEY if altPressed && !keyPressed =>
        {
          setToolMode(ToolMode.Pan)
        }
        case InputTriggers.T_ORBIT_KEY if altPressed && !keyPressed =>
        {
          setToolMode(ToolMode.Orbit)
        }
        case InputTriggers.T_ZOOM_KEY =>
        {
          if (altPressed && !keyPressed) setToolMode(ToolMode.Zoom)
          else if (cmdPressed && !keyPressed) history.undo()
        }
        case InputTriggers.T_ESCAPE_KEY if !keyPressed =>
        {
          setToolMode(ToolMode.Select)
          setViewMode(ViewMode.Isometric)
          clearSelectedNode()
        }
        case InputTriggers.T_VIEW_ISO if metaPressed && !keyPressed =>
        {
          setViewMode(Isometric)
        }
        case InputTriggers.T_VIEW_TOP if metaPressed && !keyPressed =>
        {
          setViewMode(Top)
        }
        case InputTriggers.T_VIEW_FRONT if metaPressed && !keyPressed =>
        {
          setViewMode(Front)
        }
        case InputTriggers.T_VIEW_LEFT if metaPressed && !keyPressed =>
        {
          setViewMode(Left)
        }
        case InputTriggers.T_VIEW_RIGHT if metaPressed && !keyPressed =>
        {
          setViewMode(Right)
        }
        case InputTriggers.T_VIEW_BACK if metaPressed && !keyPressed =>
        {
          setViewMode(Back)
        }
        case InputTriggers.T_REDO_KEY if cmdPressed && !keyPressed =>
        {
          history.redo()
        }
        case _ => {
          println("Unhandled action. Name='" + name + "' keyPressed=" + keyPressed +
                  " cmdPressed=" + cmdPressed + " metaPressed=" + metaPressed + " altPressed=" + altPressed)
        }
      }

      hudDisplayMouseState()
    }

    // Logic executed when the select tool is used.
    def handlePicking() {
      val results = new CollisionResults()
      // Convert screen click to 3d position
      val click2d = inputManager.getCursorPosition
      val click3d = cam.getWorldCoordinates(new Vector2f(click2d.x, click2d.y), 0f).clone()
      val dir = cam.getWorldCoordinates(new Vector2f(click2d.x, click2d.y), 1f).subtractLocal(click3d).normalizeLocal()
      // Aim the ray from the clicked spot forwards.
      val ray = new Ray(click3d, dir)
      // Collect intersections between ray and all model nodes in results list.
      models.collideWith(ray, results)
      results.zipWithIndex.foreach {
        case (r, i) =>
          val dist = r.getDistance
          val pt = r.getContactPoint
          val target = r.getGeometry.getName
      }
      if (results.size() > 0) {
        val closest = results.find { e => !e.getGeometry.getName.contains("SelectionBox") }
        closest match {
          case Some(c: CollisionResult) => clearSelectedNode(); setSelectedNode(c.getGeometry.getParent)
          case _ =>
        }
      } else {
        // No hits
        clearSelectedNode()
      }
    }
  }

  def setSelectedNode(n: Node) {
    if (selectedNode == n) // Already has selection box
      return

    hudDisplaySelectedNode("Selected: " + n.getChild(0).toString)

    val objData = n.getUserData[ObjData](ObjDataKey)

    val geom = n.getChild(0).asInstanceOf[Geometry]
    n.attachChildAt(mkSelectionBox(geom.getName, geom), 1)

    selectedNode = n
  }

  def clearSelectedNode() {
    hudDisplaySelectedNode("Selected: None")
    if (selectedNode != null) {
      selectedNode.detachChildAt(1)
      selectedNode = null
    }
  }

  def setViewMode(mode: ViewMode) {
    hudDisplayViewMode(mode)
    curViewMode = mode

    curViewMode match {
      case Isometric => rotateCamFromAngles(45, 45)
      case Top       => rotateCamFromAngles(0, 0)
      case Front     => rotateCamFromAngles(-180, -90)
      case Left      => rotateCamFromAngles(-90, -90)
      case Right     => rotateCamFromAngles(-90, 90)
      case Back      => rotateCamFromAngles(0,-90)
    }
  }

  def setToolMode(mode: ToolMode) {
    hudDisplayToolMode(mode)
    curToolMode = mode
  }

  def hudDisplayToolMode(mode: ToolMode.ToolMode) {
    hud.toolModeLabel.setText("Tool: " + mode.toString)
  }

  def hudDisplayViewMode(mode: ViewMode.ViewMode) {
    hud.viewModeLabel.setText("View: " + mode.toString)
  }

  def hudDisplayMouseState() {
    hud.mouseStateLabel.setText("LMB: " + lmbPressed + " RMB: " + rmbPressed)
  }

  def hudDisplaySelectedNode(s: String) {
    hud.selectedNodeLabel.setText(s)
  }

  def moveCameraPreserveAngles(vec: Vector3f) {
    cam.setLocation(cam.getLocation.add(vec))
  }

  def rotateCamFromAngles(myTheta: Float, myPhi: Float) {
    theta = myTheta
    phi = myPhi
    setCameraFromAngles(theta, phi)
  }

  def rotateCamFromMousePos(lastMousePos: Vector2f, curMousePos: Vector2f) {
    val deltaTheta = ((curMousePos.x - lastMousePos.x) * 0.5).toFloat
    val deltaPhi = ((curMousePos.y - lastMousePos.y) * 0.5).toFloat

    theta += deltaTheta
    phi += deltaPhi

    // Clamp phi between -180 and 180
    phi = math.min(180, math.max(-180, phi))

    setCameraFromAngles(theta, phi)
  }

  def setCameraFromAngles(myTheta: Float, myPhi: Float) {
    val camPos = new Vector3f()

    camPos.x = (curRadius * math.sin(FastMath.DEG_TO_RAD*myPhi) * math.cos(FastMath.DEG_TO_RAD*myTheta)).toFloat
    camPos.y = (curRadius * math.cos(FastMath.DEG_TO_RAD*myPhi)).toFloat
    camPos.z = (curRadius * math.sin(FastMath.DEG_TO_RAD*myPhi) * math.sin(FastMath.DEG_TO_RAD*myTheta)).toFloat

    cam.setLocation(camPos)

    cam.lookAt(new Vector3f(0f, 0f, 0f), new Vector3f(0f, 1f, 0f))
  }

}
