package acumen.ui.visualeditor.apps

import com.jme3.input.controls._
import com.jme3.input.{InputManager, KeyInput, MouseInput}

object InputTriggers {
  val T_LMB = "lmb"
  val T_RMB = "rmb"
  val T_META_KEY   = "meta_key"
  val T_ALT_KEY    = "alt_key"
  val T_CMD_KEY    = "cmd_key"
  val T_MOVE_KEY   = "move_key"
  val T_ROTATE_KEY = "rotate_key"
  val T_SCALE_KEY  = "scale_key"
  val T_SELECT_KEY = "select_key"
  val T_PAN_KEY    = "pan_key"
  val T_ORBIT_KEY  = "orbit_key"
  val T_ZOOM_KEY   = "zoom_key"
  val T_ESCAPE_KEY = "escape_key"
  val T_VIEW_ISO   = "view_iso"
  val T_VIEW_TOP   = "view_top"
  val T_VIEW_FRONT = "view_front"
  val T_VIEW_LEFT  = "view_left"
  val T_VIEW_RIGHT = "view_right"
  val T_VIEW_BACK  = "view_back"
  val T_UNDO_KEY   = "undo_key"
  val T_REDO_KEY   = "redo_key"

  val T_AXIS_X_POS = "X+"
  val T_AXIS_X_NEG = "X-"
  val T_AXIS_Y_POS = "Y+"
  val T_AXIS_Y_NEG = "Y-"
  val T_AXIS_Z_POS = "Z+"
  val T_AXIS_Z_NEG = "Z-"
}

class InputTriggers(var im: InputManager, var acl: ActionListener, var anl: AnalogListener) {
  im.addMapping(InputTriggers.T_LMB,        new MouseButtonTrigger(MouseInput.BUTTON_LEFT))
  im.addMapping(InputTriggers.T_RMB,        new MouseButtonTrigger(MouseInput.BUTTON_RIGHT))
  im.addMapping(InputTriggers.T_META_KEY,   new KeyTrigger(KeyInput.KEY_LMETA))
  im.addMapping(InputTriggers.T_ALT_KEY,    new KeyTrigger(KeyInput.KEY_LMENU))
  im.addMapping(InputTriggers.T_CMD_KEY,    new KeyTrigger(KeyInput.KEY_LCONTROL))
  im.addMapping(InputTriggers.T_MOVE_KEY,   new KeyTrigger(KeyInput.KEY_M))
  im.addMapping(InputTriggers.T_ROTATE_KEY, new KeyTrigger(KeyInput.KEY_R))
  im.addMapping(InputTriggers.T_SCALE_KEY,  new KeyTrigger(KeyInput.KEY_C))
  im.addMapping(InputTriggers.T_SELECT_KEY, new KeyTrigger(KeyInput.KEY_E))
  im.addMapping(InputTriggers.T_PAN_KEY,    new KeyTrigger(KeyInput.KEY_A))
  im.addMapping(InputTriggers.T_ORBIT_KEY,  new KeyTrigger(KeyInput.KEY_O))
  im.addMapping(InputTriggers.T_ZOOM_KEY,   new KeyTrigger(KeyInput.KEY_Z))
  im.addMapping(InputTriggers.T_ESCAPE_KEY, new KeyTrigger(KeyInput.KEY_ESCAPE))
  im.addMapping(InputTriggers.T_VIEW_ISO,   new KeyTrigger(KeyInput.KEY_1))
  im.addMapping(InputTriggers.T_VIEW_TOP,   new KeyTrigger(KeyInput.KEY_2))
  im.addMapping(InputTriggers.T_VIEW_FRONT, new KeyTrigger(KeyInput.KEY_3))
  im.addMapping(InputTriggers.T_VIEW_LEFT,  new KeyTrigger(KeyInput.KEY_4))
  im.addMapping(InputTriggers.T_VIEW_RIGHT, new KeyTrigger(KeyInput.KEY_5))
  im.addMapping(InputTriggers.T_VIEW_BACK,  new KeyTrigger(KeyInput.KEY_6))
  im.addMapping(InputTriggers.T_UNDO_KEY,   new KeyTrigger(KeyInput.KEY_Z))
  im.addMapping(InputTriggers.T_REDO_KEY,   new KeyTrigger(KeyInput.KEY_Y))
  im.addListener(acl,
    InputTriggers.T_LMB,
    InputTriggers.T_RMB,
    InputTriggers.T_META_KEY,
    InputTriggers.T_ALT_KEY,
    InputTriggers.T_CMD_KEY,
    InputTriggers.T_MOVE_KEY,
    InputTriggers.T_ROTATE_KEY,
    InputTriggers.T_SCALE_KEY,
    InputTriggers.T_SELECT_KEY,
    InputTriggers.T_PAN_KEY,
    InputTriggers.T_ORBIT_KEY,
    InputTriggers.T_ZOOM_KEY,
    InputTriggers.T_ESCAPE_KEY,
    InputTriggers.T_VIEW_ISO,
    InputTriggers.T_VIEW_TOP,
    InputTriggers.T_VIEW_FRONT,
    InputTriggers.T_VIEW_LEFT,
    InputTriggers.T_VIEW_RIGHT,
    InputTriggers.T_VIEW_BACK,
    InputTriggers.T_UNDO_KEY,
    InputTriggers.T_REDO_KEY)

  im.addMapping(InputTriggers.T_AXIS_X_POS, new MouseAxisTrigger(MouseInput.AXIS_X, false))
  im.addMapping(InputTriggers.T_AXIS_X_NEG, new MouseAxisTrigger(MouseInput.AXIS_X, true))
  im.addMapping(InputTriggers.T_AXIS_Y_POS, new MouseAxisTrigger(MouseInput.AXIS_Y, false))
  im.addMapping(InputTriggers.T_AXIS_Y_NEG, new MouseAxisTrigger(MouseInput.AXIS_Y, true))
  im.addMapping(InputTriggers.T_AXIS_Z_POS, new MouseAxisTrigger(MouseInput.AXIS_WHEEL, false))
  im.addMapping(InputTriggers.T_AXIS_Z_NEG, new MouseAxisTrigger(MouseInput.AXIS_WHEEL, true))
  im.addListener(anl,
    InputTriggers.T_AXIS_X_POS,
    InputTriggers.T_AXIS_X_NEG,
    InputTriggers.T_AXIS_Y_POS,
    InputTriggers.T_AXIS_Y_NEG,
    InputTriggers.T_AXIS_Z_POS,
    InputTriggers.T_AXIS_Z_NEG)
}
