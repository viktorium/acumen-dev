package acumen.ui.visualeditor.apps

import scala.collection.mutable.ListBuffer
import acumen._
import acumen.Init
import acumen.Lit
import acumen.ClassDef

// parent: The parent instance. None if this is the root instance of Main.
// cls: The class name of this instance
// args: Ordered list of arguments passed by the parent to the constructor of @cls.
class InstanceTree(val expr: Option[Init], val name: String, val parent: Option[InstanceTree], val cls: ClassDesc, val args: Seq[VarParam]) {
  val children = ListBuffer[InstanceTree]()

  override def toString = "Instance[" + name + "](" + args.mkString(",") + ")" + "@" + cls

  // So the application selects a 3d shape. The shape has an InstanceTree associated
  // with it, as well as an index into the _3D variable. The _3D variable is mirrored
  // in the ClassDesc's threeDs shape list.

  // Find the Literals corresponding to the parameters of this position.
  // The params can be constants, locals or constructor arguments.
  // The resulting Lit's reference the actual AST objects.
  def findTDPositionOriginVars(tdIndex: Int) : List[Lit] = {
    findPosParam(cls.threeDs(tdIndex).pos.origin)
  }

  // Find the Literals corresponding to the parameters of this position's offset.
  // If the offset isn't present, the Lit's are zeros.
  def findTDPositionOffsetVars(tdIndex: Int) : List[Lit] = {
    findPosParam(cls.threeDs(tdIndex).pos.offset)
  }

  def findPosParam(p: PosParam) : List[Lit] = {
    p match {
      case PosParamVar(v) => {
        findTDVar(v)
      }
      case PosParamVec(x,y,z) => {
        val xLit = findTDVar(x).head
        val yLit = findTDVar(y).head
        val zLit = findTDVar(z).head
        List(xLit, yLit, zLit)
      }
    }
  }

  def findTDSizeVars(tdIndex: Int) : List[Lit] = {
    val sizeParam = cls.threeDs(tdIndex).size.s
    sizeParam match {
      case SizeParamVar(v) => {
        findTDVar(v)
      }
      case SizeParamVec(l) => {
        l.map(findTDVar).flatten
      }
    }
  }

  def findTDColorVars(tdIndex: Int) : List[Lit] = {
    val colorParam = cls.threeDs(tdIndex).color.p
    colorParam match {
      case ColorParamVar(v) => {
        findTDVar(v)
      }
      case ColorParamVec(r,g,b) => {
        val rLit = findTDVar(r).head
        val gLit = findTDVar(g).head
        val bLit = findTDVar(b).head
        List(rLit, gLit, bLit)
      }
    }
  }

  // Three major cases for expressions used in a _3D shape position:
  // - A constant       \ Resolved directly through the class information
  // - A local variable /
  // - A variable passed through the constructor - Needs to examine the parent chain.
  def findTDVar(vp: VarParam) : List[Lit] = {
    vp match {
      case c: Constant => findConstant(c)
      case i: Implied => findImplied(i)
      case l: Local => findLocal(l)
      case a: Argument => findArg(a, cls.args.indexWhere(_ == a.name.x))
    }
  }

  // class Some(p)
  // a := 0;
  // x := [a, 1, 2, p];
  // Need to resolve as Constant, Local and Argument
  def findLocal(loc: Local) : List[Lit] = {
    loc.l.map(findTDVar).flatten
  }

  def findImplied(i: Implied): List[Lit] = {
    i.l.map(findTDVar).flatten
  }

  def findConstant(c: Constant) : List[Lit] = {
    List(c.lit)
  }

  // Passed as constructor argument, we need to go up the chain through the parent link.
  // The Argument carries the variable name, while the idx is the index of the constructor
  // argument with this name. For example, consider:
  //   class A() begin
  //     y := 2;
  //     b := create B(y, 3)   <<<<<<<<<<<<<<< findArg(y,0), findArg(z,1)
  //   end
  //   class B(y,z) begin      <<<<<<<<<<<<<<< cls.args
  //     _3D = ["Sphere", [0,y,z], ...]
  //   end
  // When considering the InstanceTree for "b", we'll be resolving [0,y,z] and find out
  // that y and z are arguments passed through the constructor. Further more, y is at argument
  // index 0, while z is at index 1. This mapping allows us to determine that
  //   InstanceTree.args(0) is y
  //   InstanceTree.args(1) is z
  // Further resolving of those vars will deliver the final values of y=2 and z=3.

  // a: the variable name as used in the create statement.
  // idx: the index of the variable in the constructor parameter list.
  // For example: a := create A(x,y,z) will need to call findArg with (x,0), (y,1) and (z,2).
  def findArg(a: Argument, idx: Int) : List[Lit] = {
    args(idx) match {
      case c: Constant => findConstant(c)
      case i: Implied => findImplied(i)
      case l: Local => parent.get.findLocal(l)
      case arg: Argument => {
        val par = parent.get
        val pClass = par.cls
        par.findArg(arg, pClass.args.indexWhere(_ == arg.name.x))
      }
    }
  }
}

object InstanceTree {
  // Determine if an Init object has a "create" on the rhs.
  def isClassInstantiation(init: Init) : Boolean = {
    init.rhs.isInstanceOf[NewRhs]
  }

  def extractInstance(instanceExpr: Init, parent: InstanceTree, ast: List[ClassDef], classes: Map[String, ClassDesc]) : List[InstanceTree] = {
    val name = instanceExpr.x.x
    val newRhs = instanceExpr.rhs.asInstanceOf[NewRhs]
    val className = newRhs.c.x
    val classDef = ast.find(_.name.x == className).get
    val classDesc = classes(className)
    val args = newRhs.fields.map(v => Helpers.extractParam(v, parent.cls.locals)).toSeq
    val instance = new InstanceTree(Some(instanceExpr), name, Some(parent), classDesc, args)
    val children = classDef.priv.filter(isClassInstantiation)
                                .map(ci => extractInstance(ci, instance, ast, classes))
                                .flatten
    instance.children ++= children
    instance :: children
  }

  def extractInstances(ast: List[ClassDef], classes: Map[String, ClassDesc]) : List[InstanceTree] = {
    // Manually extract main, since it is not explicitly created
    val mainClass = ast.find(b => b.name.x == "Main").get
    val main = new InstanceTree(None, "Main", None, classes("Main"), Seq())
    val children = mainClass.priv.filter(isClassInstantiation)
                                 .map(ci => extractInstance(ci, main, ast, classes))
                                 .flatten
    main.children ++= children
    main :: children
  }
}

class ClassDesc(val name: String, val args: Seq[String], val threeDs: Seq[TDShape], val locals: List[Local], val insts: List[String]) {
  override def toString = "Class[" + name + "](" + args.mkString(",") + ")"
}

object ClassDesc {
  // @ev is a list of all expressions that specify a _3D shape
  // @ev:
  //  0:Type[String]  -> Lit(GStr(Sphere))
  //  1:Pos[Numeric]  -> ExprVector(List(Lit(GInt(1)), Lit(GInt(1)), Lit(GInt(5)), Lit(GInt(0))))
  //  2:Size[Vector]  -> Lit(GDouble(0.05)) or ExprVector(List(Lit(GInt(1)), Lit(GInt(1)))
  //  3:Color[Vector] -> ExprVector(List(Lit(GDouble(0.3)), Lit(GDouble(0.7)), Lit(GDouble(0.2))))
  //  4:Angle[Vector] -> ExprVector(List(Lit(GInt(0)), Lit(GInt(0)), Lit(GInt(0))))
  //  case(Type) ->
  //    "Text" -> 5: Text string
  //    "OBJ"  -> 5: Path string
  def extractTD(ev: List[Expr], locals: List[Local]) : TDShape = {
    val shape = ev(0).asInstanceOf[Lit].gv.asInstanceOf[GStr].s
    val pos = extractTDPos(ev(1), locals)
    val size = extractTDSize(ev(2), locals)
    val color = extractTDColor(ev(3), locals)

    shape match {
      case "Box" => TDBox(pos, size, color)
      case "Sphere" => TDSphere(pos, size, color)
      case "Cylinder" => TDCylinder(pos, size, color)
//      case "Cone" => TDCone(pos, size)
//      case "Text" => TDText(pos, size)
      case _ => throw new Exception("Shape not implemented: " + shape)
    }
  }

  def extractTDPos(e: Expr, locals: List[Local]) : TDPos = {
    e match {
      case v: Var => {
        val posVar = Helpers.extractParam(v, locals)
        TDPos(PosParamVar(posVar))
      }
      case ExprVector(posVec) => {
        TDPos(PosParamVec(Helpers.extractParam(posVec(0), locals),
                          Helpers.extractParam(posVec(1), locals),
                          Helpers.extractParam(posVec(2), locals)))
      }
      // Only allowed expression is "origin + offset".
      case Op(Name("+", 0), lhs::rhs::Nil) => {
        val lhsVar = lhs match {
          case v: Var => PosParamVar(Helpers.extractParam(v, locals))
          case _ => throw new Exception("Only origin variable allowed on the lhs.")
        }
        val rhsVec = rhs match {
          case ExprVector(posVec) => {
            PosParamVec(Helpers.extractParam(posVec(0), locals),
                        Helpers.extractParam(posVec(1), locals),
                        Helpers.extractParam(posVec(2), locals))
          }
        }
        TDPos(lhsVar, rhsVec)
      }
      case _ => throw new Exception()
    }
  }

  def extractTDSize(e: Expr, locals: List[Local]) : TDSize = {
    e match {
      case l: Lit => {
        val posLit = Helpers.extractParam(l, locals)
        TDSize(SizeParamVar(posLit))
      }
      case v: Var => {
        val posVar = Helpers.extractParam(v, locals)
        TDSize(SizeParamVar(posVar))
      }
      case ExprVector(posVec) => {
        val dims = posVec.map(p => Helpers.extractParam(p, locals))
        TDSize(SizeParamVec(dims))
      }
      case _ => throw new Exception()
    }
  }

  def extractTDColor(e: Expr, locals: List[Local]) : TDColor = {
    e match {
      case v: Var => {
        val posVar = Helpers.extractParam(v, locals)
        TDColor(ColorParamVar(posVar))
      }
      case ExprVector(colorVec) => {
        TDColor(ColorParamVec(Helpers.extractParam(colorVec(0), locals),
                              Helpers.extractParam(colorVec(1), locals),
                              Helpers.extractParam(colorVec(2), locals)))
      }
      case _ => throw new Exception()
    }
  }

  // @ev is a list of ExprVectors, each of which creates a shape
  def extractTDs(ev: List[Expr], locals: List[Local]) : Seq[TDShape] = {
    ev.map(e => extractTD(e.asInstanceOf[ExprVector].l, locals)).toSeq
  }

  def extractClassDesc(cDef: ClassDef): ClassDesc = {
    val name = cDef.name.x
    val args = cDef.fields.map(_.x)
    val locals = Helpers.resolveLocals(cDef.priv, args)
    val insts = Helpers.extractInsts(cDef.priv)
    val threeDs = extractTDs(Helpers.threeDElemsFromClass(cDef), locals)
    new ClassDesc(name, args, threeDs, locals, insts)
  }
}

sealed abstract class VarParam
// Constants are local to an expression
case class Constant(lit: Lit) extends VarParam {
  def this(i: Int) = this(Lit(GInt(i)))
  def this(d: Double) = this(Lit(GDouble(d)))
}
// A local expression used implicitly, for example [1,v2,z] in "x := create X([1,v2,z])"
case class Implied(l: List[VarParam]) extends VarParam {
  def this(p: VarParam) = this(List(p))
}
// Local variables are local to the class
case class Local(name: Name, l: List[VarParam]) extends VarParam {
  def this(s: String, l: List[VarParam]) = this(Name(s, 0), l)
}
// Argument passed through the constructor
case class Argument(name: Name) extends VarParam {
  def this(s: String) = this(Name(s, 0))
}

object Constant {
  def apply(i: Int) = new Constant(i)
  def apply(d: Double) = new Constant(d)
}
object Implied {
  def apply(v: VarParam) = new Implied(List(v))
}
object Local {
  def apply(s: String, v: VarParam) = new Local(s, List(v))
  def apply(s: String, l: List[VarParam]) = new Local(s, l)
}
object Argument {
  def apply(s: String) = new Argument(s)
}

// The position parmeter can be either a variable referencing a vector:
//   vec := [1,2,3];
//   _3D := ["Box", vec, ...]
// or a vector of variables:
//   _3D := ["Box", [x,y,0], ...]
sealed abstract class PosParam
case class PosParamVar(v: VarParam) extends PosParam
case class PosParamVec(x: VarParam, y: VarParam, z: VarParam) extends PosParam

// The size parameter can be either a single value or a vector of length 2 or 3
sealed abstract class SizeParam
case class SizeParamVar(dim: VarParam) extends SizeParam
case class SizeParamVec(dims: List[VarParam]) extends SizeParam

sealed abstract class ColorParam
case class ColorParamVar(v: VarParam) extends ColorParam
case class ColorParamVec(r: VarParam, g: VarParam, b: VarParam) extends ColorParam

// Shapes have position, angle and color are all vectors with 3 numbers.
// Shapes have different sizes:
// - Box: vector with 3 numbers
// - Cylinder: vector with 2 numbers
// - Cone: vector with 2 numbers
// - Sphere: 1 number
// - Text: 1 number
case class TDPos(origin: PosParam, offset: PosParamVec) {
  def this(origin: PosParam) = this(origin, PosParamVec(Constant(0), Constant(0), Constant(0)))
  def this(x: VarParam, y: VarParam, z: VarParam) = this(PosParamVec(x,y,z))
  def this(origin: VarParam) = this(PosParamVar(origin))
}
object TDPos {
  def apply(x: VarParam, y: VarParam, z: VarParam) = new TDPos(PosParamVec(x,y,z))
  def apply(origin: PosParam) = new TDPos(origin)
  def apply(origin: VarParam) = new TDPos(PosParamVar(origin))
}
case class TDSize(s: SizeParam)
object TDSize {
  def apply(i: Int) = new TDSize(SizeParamVar(Constant(i)))
  def apply(i1: Int, i2: Int) = new TDSize(SizeParamVec(List(Constant(i1), Constant(i2))))
  def apply(i1: Int, i2: Int, i3: Int) = new TDSize(SizeParamVec(List(Constant(i1), Constant(i2), Constant(i3))))
}
case class TDColor(p: ColorParam)
object TDColor {
  def apply(r: Float, g: Float, b: Float) = new TDColor(ColorParamVec(Constant(r), Constant(g), Constant(b)))
  def Red = TDColor(1,0,0)
  def Green = TDColor(0,1,0)
  def Blue = TDColor(0,0,1)
  def Black = TDColor(0,0,0)
  def White = TDColor(1,1,1)
}
case class TDAngle

sealed abstract class TDShape(val pos: TDPos, val size: TDSize, val color: TDColor)
case class TDBox(override val pos: TDPos, override val size: TDSize, override val color: TDColor) extends TDShape(pos, size, color)
case class TDSphere(override val pos: TDPos, override val size: TDSize, override val color: TDColor) extends TDShape(pos, size, color)
case class TDCylinder(override val pos: TDPos, override val size: TDSize, override val color: TDColor) extends TDShape(pos, size, color)

