package acumen.ui.visualeditor.apps

object ViewMode extends Enumeration {
  type ViewMode = Value
  val Isometric = Value("Isometric")
  val Top = Value("Top")
  val Front = Value("Front")
  val Left = Value("Left")
  val Right = Value("Right")
  val Back = Value("Back")
}
