package acumen.ui.visualeditor.apps

object ToolMode extends Enumeration {
  type ToolMode = Value
  val Select = Value("Select")
  val Pan = Value("Pan")
  val Orbit = Value("Orbit")
  val Zoom = Value("Zoom")

  val Move = Value("Move")
  val Rotate = Value("Rotate")
  val Scale = Value("Scale")
}
