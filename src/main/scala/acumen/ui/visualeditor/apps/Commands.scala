package acumen.ui.visualeditor.apps

import scala.collection.mutable
import com.jme3.scene.Node
import com.jme3.math.{Quaternion, Vector3f}

trait Command {
  def execute(): Unit
  def undo(): Unit
}

class RotateParams
class ScaleParams

class MoveCommand(app: SimpleApp, target: Node, newLoc: Vector3f, origLoc: Vector3f) extends Command {
  def execute() { target.setLocalTranslation(newLoc) }
  def undo()    { target.setLocalTranslation(origLoc) }
  override def toString: String = { "MoveCmd" }
}

class RotateCommand(app: SimpleApp, target: Node, newRot: Quaternion, origRot: Quaternion) extends Command {
  def execute() { target.setLocalRotation(newRot) }
  def undo()    { target.setLocalRotation(origRot) }
  override def toString: String = { "RotateCmd" }
}

class ScaleCommand(app: SimpleApp, target: Node, newScale: Vector3f, origScale: Vector3f) extends Command {
  def execute() { target.setLocalScale(newScale) }
  def undo()    { target.setLocalScale(origScale) }
  override def toString: String = { "ScaleCmd" }
}

class CommandHistory {
  val undoHistory = mutable.ArrayStack[Command]()
  val redoHistory = mutable.ArrayStack[Command]()

  def add(cmd: Command) {
    undoHistory.push(cmd)
    if (redoHistory.length > 0)
      redoHistory.clear()
//    println(this)
  }

  def undo() {
    if (undoHistory.length > 0) {
      redoHistory.push(undoHistory.pop())
      redoHistory.head.undo()
    }
  }

  def redo() {
    if (redoHistory.length > 0) {
      undoHistory.push(redoHistory.pop())
      undoHistory.head.execute()
    }
  }

  def clear() {
    undoHistory.clear()
    redoHistory.clear()
  }

  def stackToString(stack: mutable.ArrayStack[Command]): String = {
    stack.foldLeft(new StringBuilder) {
      (acc, cmd) => acc.append(cmd.toString + ", ")
    }.toString()
  }

  override def toString: String = {
    "Undo: Top[" + stackToString(undoHistory) + "]Bottom\r\n" +
    "Redo: Top[" + stackToString(redoHistory) + "]Bottom"
  }
}