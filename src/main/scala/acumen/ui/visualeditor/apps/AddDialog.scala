package acumen.ui.visualeditor.apps

import scala.swing._
import scala.swing.BorderPanel.Position
import scala.Some

class AddDialog(val shape: String, val classes: List[String])  extends Dialog {
  var whichClass: Option[String] = None

  title = "Add to Parent"
  modal = true

  contents = new BorderPanel {
    val label = new Label("Add a " + shape + " to class: ")
    val comboBox = new ComboBox(classes)

    val choice = new BoxPanel(Orientation.Horizontal) {
      contents += label
      contents += comboBox
    }
    layout(choice) = Position.North

    layout(new FlowPanel(FlowPanel.Alignment.Right)(
      new Button(Action("OK") {
        whichClass = if (comboBox.selection.index == -1) None else Some(comboBox.selection.item)
        close()
      }),
      new Button(Action("Cancel") {
        whichClass = None
        close()
      }))) = Position.South
  }

  centerOnScreen()
  open()
}
