package acumen.ui.visualeditor.apps

import com.jme3.font.BitmapText
import com.jme3.app.SimpleApplication
import com.jme3.math.ColorRGBA

class Hud(private val app: SimpleApplication) {
  private val hudFont = app.getAssetManager.loadFont("Interface/Fonts/Default.fnt")
  private val xOffset = 5
  val viewModeLabel = new BitmapText(hudFont, false)
  val toolModeLabel = new BitmapText(hudFont, false)
  val selectedNodeLabel = new BitmapText(hudFont, false)
  val mouseStateLabel = new BitmapText(hudFont, false)

  viewModeLabel.setSize(hudFont.getCharSet.getRenderedSize)
  viewModeLabel.setLocalTranslation(xOffset, viewModeLabel.getLineHeight, 0)
  viewModeLabel.setColor(ColorRGBA.Black)

  toolModeLabel.setSize(hudFont.getCharSet.getRenderedSize)
  toolModeLabel.setLocalTranslation(xOffset, viewModeLabel.getLineHeight * 2f, 0)
  toolModeLabel.setColor(ColorRGBA.Black)

  selectedNodeLabel.setSize(hudFont.getCharSet.getRenderedSize)
  selectedNodeLabel.setLocalTranslation(xOffset, viewModeLabel.getLineHeight * 3f, 0)
  selectedNodeLabel.setColor(ColorRGBA.Black)

  mouseStateLabel.setSize(hudFont.getCharSet.getRenderedSize)
  mouseStateLabel.setLocalTranslation(xOffset, viewModeLabel.getLineHeight * 4f, 0)
  mouseStateLabel.setColor(ColorRGBA.Black)

  app.getGuiNode.detachAllChildren()
  app.getGuiNode.attachChild(viewModeLabel)
  app.getGuiNode.attachChild(toolModeLabel)
  app.getGuiNode.attachChild(selectedNodeLabel)
  app.getGuiNode.attachChild(mouseStateLabel)
}
