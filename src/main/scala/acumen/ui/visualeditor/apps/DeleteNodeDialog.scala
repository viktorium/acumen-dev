package acumen.ui.visualeditor.apps

import scala.swing._
import scala.swing.BorderPanel._

sealed abstract class DeleteDialogResult
case class DeleteNode() extends DeleteDialogResult
case class DeleteParent() extends DeleteDialogResult

class DeleteNodeDialog(val shapeName: String, val creatorName: String, val parName: String) extends Dialog {
  var deleteWhat: Option[DeleteDialogResult] = None

  title = "Delete Node or Parent"
  modal = true

  contents = new BorderPanel {
    val mutex = new ButtonGroup
    val delNode = new RadioButton("Delete the selected " + shapeName + " inside _3D section of object " + creatorName)
    val delParent = new RadioButton("Delete the create statement in " + parName)
    val radios = List(delNode, delParent)
    mutex.buttons ++= radios
    mutex.select(delNode)

    if (creatorName == "Main" && parName == "Main") delParent.enabled = false

    val buttons = new BoxPanel(Orientation.Vertical) {
      contents ++= radios
    }
    layout(buttons) = Position.North

    layout(new FlowPanel(FlowPanel.Alignment.Right)(
      new Button(Action("Delete it!") {
        mutex.selected.get match {
          case `delNode` =>
            deleteWhat = Some(DeleteNode())
          case `delParent` =>
            deleteWhat = Some(DeleteParent())
        }
        close()
      }),
      new Button(Action("Cancel") {
        deleteWhat = None
        close()
      }))) = Position.South
  }

  centerOnScreen()
  open()
}
