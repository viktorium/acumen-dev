package acumen.ui.visualeditor.apps

import com.jme3.math.ColorRGBA
import java.util.Random
import acumen._
import acumen.ExprVector
import acumen.GDouble
import acumen.Init
import acumen.Lit
import acumen.ClassDef
import scala.Some
import acumen.ExprRhs
import scala.collection.mutable.ListBuffer
import javax.vecmath.Vector3d

object Helpers {
  val rnd = new Random()

  // Create a ColorRGBA object from an integer.
  def fromRGB(hex: Int): ColorRGBA =
    fromRGB(((hex>>16)&0xFF).toShort, ((hex>>8)&0xFF).toShort, (hex&0xFF).toShort)

  // Create a ColorRGBA object from rgb integer components.
  def fromRGB(r: Short, g: Short, b: Short): ColorRGBA =
    new ColorRGBA(r/255.0f, g/255.0f, b/255.0f, 1.0f)

  def randomRGB(): ColorRGBA = {
    val red   = (rnd.nextFloat() + ColorRGBA.White.getRed) / 2
    val green = (rnd.nextFloat() + ColorRGBA.White.getGreen) / 2
    val blue  = (rnd.nextFloat() + ColorRGBA.White.getBlue) / 2

    new ColorRGBA(red, green, blue, 1.0f)
  }

  def threeDElemsFromClass(cls: ClassDef): List[Expr] = {
    cls.priv.find(i => i.x.x == "_3D") match {
      case init: Some[Init] => {
        val vecElems = init.get.rhs.asInstanceOf[ExprRhs].e.asInstanceOf[ExprVector]
        // If the _3d contains only one shape, the expression will not be wrapped in a List.
        // In that case wrap it manually.
        if (vecElems.l.length == 0)
          List()
        else if (!vecElems.l(0).isInstanceOf[ExprVector])
          List(init.get.rhs.asInstanceOf[ExprRhs].e)
        else
          vecElems.l
      }
      // _3D is not present
      case _ => List()
    }
  }

  def getSize(l: List[Lit]): Vector3d = {
    val nums = l.map(a => getNumericValue(a.gv))
    l.length match {
      case 1 => new Vector3d(nums(0), nums(0), nums(0)) // Sphere, Text
      case 2 => new Vector3d(nums(0), nums(1), 1f) // Cylinder, Cone
      case 3 => new Vector3d(nums(0), nums(1), nums(2)) // Box
      case _ => throw new Exception("Unexpected number of literals for size: " + l)
    }
  }

  def getPos(pos: List[Lit]): Vector3d = {
    val xComp = getNumericValue(pos(0).gv)
    val yComp = getNumericValue(pos(1).gv)
    val zComp = getNumericValue(pos(2).gv)
    new Vector3d(xComp, yComp, zComp)
  }

  def getPos(expr: Expr): Vector3d = {
    val vec = expr.asInstanceOf[ExprVector].l
    val xComp = getNumericValue(vec(0).asInstanceOf[Lit].gv)
    val yComp = getNumericValue(vec(1).asInstanceOf[Lit].gv)
    val zComp = getNumericValue(vec(2).asInstanceOf[Lit].gv)
    new Vector3d(xComp, yComp, zComp)
  }

  def getColor(expr: Expr): ColorRGBA = {
    val vec = expr.asInstanceOf[ExprVector].l
    val xComp = vec(0).asInstanceOf[Lit].gv.asInstanceOf[GDouble].d.toFloat
    val yComp = vec(1).asInstanceOf[Lit].gv.asInstanceOf[GDouble].d.toFloat
    val zComp = vec(2).asInstanceOf[Lit].gv.asInstanceOf[GDouble].d.toFloat
    new ColorRGBA(xComp, yComp, zComp, 0)
  }

  def getColor(pos: List[Lit]): ColorRGBA = {
    val rComp = getNumericValue(pos(0).gv).toFloat
    val gComp = getNumericValue(pos(1).gv).toFloat
    val bComp = getNumericValue(pos(2).gv).toFloat
    new ColorRGBA(rComp, gComp, bComp, 0f)
  }

  def getNumericValue(vp: VarParam): Double = {
    vp match {
      case c:Constant => getNumericValue(c.lit.gv)
      case _ => throw new Exception("Expecting constant, got " + vp)
    }
  }

  def getNumericValue(gv: GroundValue): Double = {
    gv match {
      case gd:GDouble => gd.d
      case gi:GInt => gi.i
      case _ => throw new Exception("Expecting numeric value, got " + gv)
    }
  }



  def resolveLit(l: Lit): List[VarParam] = {
    List(Constant(l))
  }

  def resolveVar(v: Var, args: List[String], locals: List[Local]): List[VarParam] = {
    if (locals.exists(_.name == v.name))
      return locals.find(_.name == v.name).get.l
    if (args.contains(v.name.x))
      return List(Argument(v.name.x))
    throw new Exception("Var is resolvable, but can not be found in args or locals.")
  }

  def resolveVector(ev: ExprVector, args: List[String], locals: List[Local]): List[VarParam] = {
    ev.l.map(elem => resolveParam(elem, args, locals)).flatten
  }

  def resolveParam(e: Expr, args: List[String], locals: List[Local]): List[VarParam] = {
    e match {
      case l: Lit => resolveLit(l)
      case v: Var => resolveVar(v, args, locals)
      case ev: ExprVector => resolveVector(ev, args, locals)
      case _ => throw new Exception("Unknown param expression: " + e)
    }
  }

  def resolveLocal(init: Init, args: List[String], locals: List[Local]): Local = {
    val varName = init.x
    val varParams = resolveParam(init.rhs.asInstanceOf[ExprRhs].e, args, locals)
    Local(varName, varParams)
  }

  // Decides whether the rhs can be resolved based on the
  // currently known locals.
  // A variable is resolvable when it is:
  //  - a Literal (constant)
  //  - a variable already resolved in locals
  //  - a vector that contains literals and variables that are already resolved
  def isResolvable(expr: Expr, args: List[String], locals: List[Local]): Boolean = {
    expr match {
      case ev: ExprVector => ev.l.forall(e => isResolvable(e, args, locals))
      case v: Var => locals.exists(_.name.x == v.name.x) || args.contains(v.name.x)
      case l: Lit => true
      case _ => false
    }
  }

  def resolveLocals(priv: List[Init], args: List[String]) : List[Local] = {
    val assignments = ListBuffer[Init](priv: _*)
    val locals = ListBuffer[Local]()

    // Remove any assignments that shouldn't be treated as local assignment
    assignments --= assignments.filter(a => a.x.x == "_3D")
    assignments --= assignments.filter(a => !a.rhs.isInstanceOf[ExprRhs])

    while (assignments.nonEmpty) {
      val resolvable = assignments.filter(a => isResolvable(a.rhs.asInstanceOf[ExprRhs].e, args, locals.toList))

      if (resolvable.size == 0)
        throw new Exception("There are assignments left to resolve, but none is resolvable")

      locals ++= resolvable.map(r => resolveLocal(r, args, locals.toList))
      assignments --= resolvable
    }

    locals.toList
  }

  // The locals are needed to determine if a variable is a constructor
  // parameter or a local one.
  def extractParam(e: Expr, locals: List[Local]) : VarParam = {
    val localNames = locals.map(_.name.x).toList
    e match {
      case v: Var => {
        if (localNames.contains(v.name.x)) locals.find(_.name == v.name).get
        else Argument(v.name)
      }
      case l: Lit => Constant(l)
      case ExprVector(l) => Implied(l.map(elem => extractParam(elem, locals)).toList)
      case _ => throw new Exception("Unknown param expression: " + e)
    }
  }

  // Extracts the variable names (lhs) of all "create" statements.
  def extractInsts(inits: List[Init]) : List[String] = {
    inits.filter(_.rhs.isInstanceOf[NewRhs]).map(_.x.x)
  }

}
