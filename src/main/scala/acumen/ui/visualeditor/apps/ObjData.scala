package acumen.ui.visualeditor.apps

import com.jme3.export.{JmeExporter, JmeImporter, Savable}
import acumen._
import acumen.Lit
import acumen.GDouble

class ObjData(val name: String, val inst: InstanceTree, val idx: Int, val origin: List[Lit], val offset: List[Lit],
              val size: List[Lit], val color: List[Lit]) extends Savable {
  override def toString = "Class=" + inst.cls + "(" + idx + ") " + inst.cls.threeDs(idx)

  // This method is called with jMonkey coordinates. Converting them
  // to Acumen model coordinates is done by:
  // a.x = j.x, a.y = -j.z, a.z = j.y
  // Every ObjData object has an offset that's added to origin. This way multiple
  // nodes can share a common origin but have different offsets. If the offset is
  // not present, it will be simply zeroes.
  def updatePos(x: Float, y: Float, z: Float) = {
    val aX = x
    val aY = -z
    val aZ = y
    setValue(origin(0), aX - Helpers.getNumericValue(offset(0).gv))
    setValue(origin(1), aY - Helpers.getNumericValue(offset(1).gv))
    setValue(origin(2), aZ - Helpers.getNumericValue(offset(2).gv))
  }

  def updateSize(l: List[Float]) = {
    size.zip(l).map(z => setValue(z._1, z._2))
  }

  def updateColor(r: Float, g: Float, b: Float) = {
    setValue(color(0), r)
    setValue(color(1), g)
    setValue(color(2), b)
  }

  def setValue(lit: Lit, value: Double) = {
    // check if the value is close to an integer
    if (math.ceil(value) == value)
      lit.gv = GInt(value.toInt)
    else
      lit.gv = GDouble(value.formatted("%.2f").toDouble)
  }

  def write(exporter: JmeExporter) {}

  def read(exporter: JmeImporter) {}
}
