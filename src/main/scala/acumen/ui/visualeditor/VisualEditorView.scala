package acumen.ui.visualeditor

import com.jme3.system.{JmeCanvasContext, AppSettings}
import com.jme3.app.{SimpleApplication, Application}
import java.awt.{Cursor, Canvas}
import java.util.concurrent.Callable
import scala.swing.Panel
import java.awt.event.{MouseEvent, MouseListener}
import acumen.ui.visualeditor.apps.SimpleApp

class VisualEditorView extends Panel {

  var context: JmeCanvasContext = null
  var canvas: Canvas = null
  var app: SimpleApp = null
  val appClass = "acumen.ui.visualeditor.apps.SimpleApp"

  createCanvas(appClass)

  def createCanvas(appClass: String) {
    val settings = new AppSettings(true)
    settings.setWidth(640)
    settings.setHeight(480)
    settings.setFrameRate(60)
    settings.setSamples(16)

    try {
      val clazz = Class.forName(appClass)
      app = clazz.newInstance().asInstanceOf[SimpleApp]
    } catch {
      case nf: ClassNotFoundException => nf.printStackTrace()
      case ie: InstantiationException => ie.printStackTrace()
      case ia: IllegalAccessException => ia.printStackTrace()
    }

    app.setPauseOnLostFocus(false)
    app.setSettings(settings)
    app.createCanvas()

    context = app.getContext.asInstanceOf[JmeCanvasContext]

    canvas = context.getCanvas
    canvas.setSize(settings.getWidth, settings.getHeight)
    canvas.addMouseListener(new MouseListener {
      def mouseExited(e: MouseEvent) {
        canvas.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR))
      }

      def mouseEntered(e: MouseEvent) {
        canvas.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR))
      }

      def mouseClicked(e: MouseEvent) {}
      def mousePressed(e: MouseEvent) {}
      def mouseReleased(e: MouseEvent) {}
    })
  }

  def startApp() {
    app.startCanvas()
    app.enqueue(new Callable[Unit](){
      def call {
        if (app.isInstanceOf[SimpleApplication]){
          val simpleApp = app.asInstanceOf[SimpleApplication]
          simpleApp.getFlyByCamera.setDragToRotate(true)
          simpleApp.getFlyByCamera.setEnabled(false)
        }
      }
    })
  }
}
