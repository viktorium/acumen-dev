package acumen.ui.visualeditor

import org.scalatest._
import acumen.ui.visualeditor.apps._
import acumen.ui.visualeditor.apps.TDSphere
import acumen.ui.visualeditor.apps.Constant
import acumen.ui.visualeditor.apps.TDBox
import acumen.ui.visualeditor.apps.TDCylinder
import acumen.ui.visualeditor.apps.Local
import acumen.ui.visualeditor.apps.TDPos
import acumen.ui.visualeditor.apps.Argument
import acumen.{GDouble, Lit, GInt}
import acumen.Parser


class InstanceTreeTests extends Suite with BeforeAndAfter {

  before {
    println(">"*50)
  }
  after {
    println("<"*50)
  }

  implicit def intToListLit(i: Int) : List[Lit] = {
    List(Lit(GInt(i)))
  }

  def test() {
    val tdbox = TDBox(TDPos(Argument("x"), Constant(0), Constant(0)), TDSize(1, 1, 1),TDColor.Blue)
    val tdsphere = TDSphere(TDPos(Constant(2), Argument("y"), Local("z", Constant(4))), TDSize(1), TDColor.Green)

    val classes = Map("Main" -> new ClassDesc("Main", Seq(), Seq(), List(Local("x", List(Constant(1)))), List()),
                      "A"    -> new ClassDesc("A", Seq("x", "y"), Seq(tdbox, tdsphere), List(Local("z", List(Constant(4)))), List()))

    val main = new InstanceTree(None, "main", None, classes("Main"), Seq())
    val a1 = new InstanceTree(None, "a1", Some(main), classes("A"), Seq(Local("x", List(Constant(4))), Constant(3)))

    println (a1.findTDPositionOriginVars(1))
  }

  def testFlat() {
//    """class Main(simulator)
//      |  private
//      |    e := 0;
//      |    f := 0;
//      |    g := 0;
//      |    _3D := ["Sphere", [e, f, g], 0.05, [0.3, 0.7, 0.2], [0, 0, 0]]
//      |  end
//      |end
//      |"""
//    val tdsphere = TDSphere(TDPos(Local("e"), Local("f"), Local("g")))
//    val classes = Map("Main" -> new ClassDesc("Main", Seq(), Seq(tdsphere), Map(Local("e") -> 0, Local("f") -> 0, Local("g") -> 0)))
//    val main = new InstanceTree("main", None, classes("Main"), Seq())
//    println("Walid0: " + main.findTDPositionVars(0))
  }

  def testNested1() {
    """class Main(simulator)
      |  private
      |    a := create Sphere(1,2,3);
      |    b := create Sphere(4,5,6);
      |    c := create Sphere(7,8,9);
      |  end
      |end
      |
      |class Sphere(x,y,z)
      |  private
      |    _3D := ["Sphere", [x, y, z], 0.05, [0.3, 0.7, 0.2], [0, 0, 0]]
      |  end
      |end
      |"""

    val tdsphere = TDSphere(TDPos(Argument("x"), Argument("y"), Argument("z")), TDSize(1), TDColor.Red)
    val classes = Map("Main" -> new ClassDesc("Main", Seq(), Seq(), List(), List()),
                      "Sphere" -> new ClassDesc("Sphere", Seq("x", "y", "z"), Seq(tdsphere), List(), List()))
    val main = new InstanceTree(None, "main", None, classes("Main"), Seq())
    val a = new InstanceTree(None, "a", Some(main), classes("Sphere"), Seq(Constant(1), Constant(2), Constant(3)))
    val b = new InstanceTree(None, "b", Some(main), classes("Sphere"), Seq(Constant(4), Constant(5), Constant(6)))
    val c = new InstanceTree(None, "c", Some(main), classes("Sphere"), Seq(Constant(7), Constant(8), Constant(9)))

    println("Walid1: " + a.findTDPositionOriginVars(0))
    println("Walid1: " + b.findTDPositionOriginVars(0))
    println("Walid1: " + c.findTDPositionOriginVars(0))
  }

  def testNested2() {
    """class Main(simulator)
      |  private
      |    a1 := create A(1);
      |    a2 := create A(2);
      |  end
      |end
      |class A(p)
      |  private
      |    b1 := create B(p,1);
      |    b2 := create B(p,2);
      |  end
      |end
      |class B(m,n)
      |  private
      |    _3D := [["Box", [m,n,0],...],
      |            ["Sphere", [n,m,1], ...];
      |    c1 := create C(m,n,1);
      |    c2 := create C(m,n,2);
      |  end
      |end
      |class C(x,y,z)
      |  private
      |    _3D := [["Cylinder", [x,y,z], ...],
      |            ["Box", [z,x,y], ...]];
      |  end
      |end
    """.stripMargin

    val tdbox = TDBox(TDPos(Argument("m"), Argument("n"), Constant(0)), TDSize(1,2,2), TDColor.Red)
    val tdsphere = TDSphere(TDPos(Argument("n"), Argument("m"), Constant(1)), TDSize(1), TDColor.Green)
    val tdcyl = TDCylinder(TDPos(Argument("x"), Argument("y"), Argument("z")), TDSize(1,2), TDColor.Blue)
    val tdb = TDBox(TDPos(Argument("z"), Argument("x"), Argument("y")), TDSize(1,2,3), TDColor.Black)

    val classes = Map("Main" -> new ClassDesc("Main", Seq(), Seq(), List(), List()),
                      "A"    -> new ClassDesc("A", Seq("p"), Seq(), List(), List()),
                      "B"    -> new ClassDesc("B", Seq("m", "n"), Seq(tdbox, tdsphere), List(), List()),
                      "C"    -> new ClassDesc("C", Seq("x", "y", "z"), Seq(tdcyl, tdb), List(), List()))

    val main = new InstanceTree(None, "main", None, classes("Main"), Seq())

    val a1 = new InstanceTree(None, "a1", Some(main), classes("A"), Seq(Constant(1)))
    val a2 = new InstanceTree(None, "a2", Some(main), classes("A"), Seq(Constant(2)))

    val a1b1 = new InstanceTree(None, "b1", Some(a1), classes("B"), Seq(Argument("p"), Constant(1)))
    val a1b2 = new InstanceTree(None, "b2", Some(a1), classes("B"), Seq(Argument("p"), Constant(2)))
    val a2b1 = new InstanceTree(None, "b1", Some(a2), classes("B"), Seq(Argument("p"), Constant(1)))
    val a2b2 = new InstanceTree(None, "b2", Some(a2), classes("B"), Seq(Argument("p"), Constant(2)))

    val a1b1c1 = new InstanceTree(None, "c1", Some(a1b1), classes("C"), Seq(Argument("m"), Argument("n"), Constant(1)))
    val a1b1c2 = new InstanceTree(None, "c2", Some(a1b1), classes("C"), Seq(Argument("m"), Argument("n"), Constant(2)))

    val a1b2c1 = new InstanceTree(None, "c1", Some(a1b2), classes("C"), Seq(Argument("m"), Argument("n"), Constant(1)))
    val a1b2c2 = new InstanceTree(None, "c2", Some(a1b2), classes("C"), Seq(Argument("m"), Argument("n"), Constant(2)))

    val a2b1c1 = new InstanceTree(None, "c1", Some(a2b2), classes("C"), Seq(Argument("m"), Argument("n"), Constant(1)))
    val a2b1c2 = new InstanceTree(None, "c2", Some(a2b1), classes("C"), Seq(Argument("m"), Argument("n"), Constant(2)))

    val a2b2c1 = new InstanceTree(None, "c1", Some(a2b2), classes("C"), Seq(Argument("m"), Argument("n"), Constant(1)))
    val a2b2c2 = new InstanceTree(None, "c2", Some(a2b2), classes("C"), Seq(Argument("m"), Argument("n"), Constant(2)))

    println("a1b1(0): " + a1b1.findTDPositionOriginVars(0))
    println("a1b1(1): " + a1b1.findTDPositionOriginVars(1))
    println("a1b2(0): " + a1b2.findTDPositionOriginVars(0))
    println("a1b2(1): " + a1b2.findTDPositionOriginVars(1))
    println("a2b1(0): " + a2b1.findTDPositionOriginVars(0))
    println("a2b1(1): " + a2b1.findTDPositionOriginVars(1))
    println("a2b2(0): " + a2b2.findTDPositionOriginVars(0))
    println("a2b2(1): " + a2b2.findTDPositionOriginVars(1))

    println("a1b1c1(0): " + a1b1c1.findTDPositionOriginVars(0))
    println("a1b1c1(1): " + a1b1c1.findTDPositionOriginVars(1))
    println("a1b1c2(0): " + a1b1c2.findTDPositionOriginVars(0))
    println("a1b1c2(1): " + a1b1c2.findTDPositionOriginVars(1))

    println("a1b2c1(0): " + a1b2c1.findTDPositionOriginVars(0))
    println("a1b2c1(1): " + a1b2c1.findTDPositionOriginVars(1))
    println("a1b2c2(0): " + a1b2c2.findTDPositionOriginVars(0))
    println("a1b2c2(1): " + a1b2c2.findTDPositionOriginVars(1))
  }

  def testClassAnalyzer() {
    val progText = """class Main(simulator)
                     |  private
                     |    a1 := create A(1);
                     |    a2 := create A(2);
                     |  end
                     |end
                     |class A(p)
                     |  private
                     |    v := 1;
                     |    b1 := create B(p,1);
                     |    b2 := create B(p,2);
                     |  end
                     |end
                     |class B(m,n)
                     |  private
                     |    _3D := [["Box", [m,n,0], [1,1,1], [0.3, 0.7, 0.2], [0, 0, 0]],
                     |            ["Sphere", [n,m,1], 0.05, [0.3, 0.7, 0.2], [0, 0, 0]]];
                     |    c1 := create C(m,n,1);
                     |    c2 := create C(m,n,2);
                     |  end
                     |end
                     |class C(x,y,z)
                     |  private
                     |    _3D := [["Cylinder", [x,y,z], [1,1], [0.3, 0.7, 0.2], [0, 0, 0]],
                     |            ["Box", [z,x,y], [1,1,1], [0.3, 0.7, 0.2], [0, 0, 0]]];
                     |  end
                     |end""".stripMargin
    val ast = Parser.run(Parser.prog, progText)
    val classes = ast.defs.map(c => c.name.x -> ClassDesc.extractClassDesc(c))
    classes
  }

  def testClassAnalyzer1() {
    val progText = """class Main(simulator)
                     |  private
                     |    a := create Sphere(1,2,3);
                     |    b := create Sphere(4,5,6);
                     |    c := create Sphere(7,8,9);
                     |  end
                     |end
                     |
                     |class Sphere(q,w,m)
                     |  private
                     |    x := 10;
                     |    _3D := ["Sphere", [x, 1, m], 0.05, [0.3, 0.7, 0.2], [0, 0, 0]]
                     |  end
                     |end
                     |""".stripMargin
    val ast = Parser.run(Parser.prog, progText)
    val classes = ast.defs.map(c => c.name.x -> ClassDesc.extractClassDesc(c))
    classes

    val instances = ast.defs.map(c => InstanceTree)
  }

  def testClassInstanceTree() {
    val progText = """class Main(simulator)
                     |  private
                     |    a1 := create A(1);
                     |    a2 := create A(2);
                     |  end
                     |end
                     |class A(p)
                     |  private
                     |    v := 1;
                     |    b1 := create B(p,3);
                     |    b2 := create B(p,4);
                     |  end
                     |end
                     |class B(m,n)
                     |  private
                     |    _3D := [["Box", [m,n,100], [1,1,1], [0.3, 0.7, 0.2], [0, 0, 0]],
                     |            ["Sphere", [n,m,200], 0.05, [0.3, 0.7, 0.2], [0, 0, 0]]];
                     |    c1 := create C(m,n,50);
                     |    c2 := create C(m,n,70);
                     |  end
                     |end
                     |class C(x,y,z)
                     |  private
                     |    _3D := [["Cylinder", [x,y,z], [1,1], [0.3, 0.7, 0.2], [0, 0, 0]],
                     |            ["Box", [z,x,y], [1,1,1], [0.3, 0.7, 0.2], [0, 0, 0]]];
                     |  end
                     |end""".stripMargin
    val ast = Parser.run(Parser.prog, progText)
    val classes = ast.defs.map(c => c.name.x -> ClassDesc.extractClassDesc(c)).toMap
    val instances = InstanceTree.extractInstances(ast.defs, classes)

    instances.filter(_.name == "c2").map(_.findTDPositionOriginVars(0)).map(println)
    instances.filter(_.name.startsWith("b")).map(_.findTDPositionOriginVars(0)).map(println)
  }

  def testClassInstanceTree1() {
    val progText = """class Main(simulator)
                     |  private
                     |    a := create Sphere(1,2,3);
                     |    b := create Sphere(4,5,6);
                     |    c := create Sphere(7,8,9);
                     |  end
                     |end
                     |
                     |class Sphere(q,w,m)
                     |  private
                     |    x := 10;
                     |    _3D := ["Sphere", [x, 1, m], 0.05, [0.3, 0.7, 0.2], [0, 0, 0]]
                     |  end
                     |end
                     |""".stripMargin
    val ast = Parser.run(Parser.prog, progText)
    val classes = ast.defs.map(c => c.name.x -> ClassDesc.extractClassDesc(c)).toMap
    val instances = InstanceTree.extractInstances(ast.defs, classes)

    instances.filter(_.name == "a").map(_.findTDPositionOriginVars(0)).map(println)
    instances.filter(_.name == "b").map(_.findTDPositionOriginVars(0)).map(println)
    instances.filter(_.name == "c").map(_.findTDPositionOriginVars(0)).map(println)
  }

  def testVectorParam() {
    val progText = """class Main(simulator)
                     |  private
                     |    z := 123;
                     |    vec := [z,20,30];
                     |    p := create Proxy(vec);
                     |  end
                     |end
                     |class Proxy(pv)
                     |  private
                     |    vecTest := create VecTest(pv);
                     |  end
                     |end
                     |class VecTest(pos)
                     |  private
                     |    _3D := ["Cylinder", pos, [0.5, 5], [0.1, 0.2, 0.3], [0, 0, 0]];
                     |  end
                     |end""".stripMargin
    val ast = Parser.run(Parser.prog, progText)
    val classes = ast.defs.map(c => c.name.x -> ClassDesc.extractClassDesc(c)).toMap
    val instances = InstanceTree.extractInstances(ast.defs, classes)
    instances.filter(_.name == "vecTest").map(_.findTDPositionOriginVars(0)).map(println)
  }

  def testLocalAggregate() {
    val progText = """class Main(simulator)
                     |  private
                     |    val1 := 1;
                     |    vref := val1;
                     |    vec := [vref,val1,30];
                     |    vecref := vec;
                     |    p := create Proxy(vec, val1, vref);
                     |  end
                     |end
                     |class Proxy(pv,v1,v2)
                     |  private
                     |    val := v1;
                     |    vecTest := create VecTest(pv,v,v1,v2);
                     |  end
                     |end
                     |class VecTest(pos,a,b,c)
                     |  private
                     |    _3D := ["Box", pos, [a, b, c], [0.1, 0.2, 0.3], [0, 0, 0]];
                     |  end
                     |end""".stripMargin
    val ast = Parser.run(Parser.prog, progText)
    val classes = ast.defs.map(c => c.name.x -> ClassDesc.extractClassDesc(c)).toMap
    val instances = InstanceTree.extractInstances(ast.defs, classes)
    instances.filter(_.name == "vecTest").map(_.findTDPositionOriginVars(0)).map(println)
  }

  def testLocalAggregate1() {
    val progText = """class Main(simulator)
                     |  private
                     |    val1 := 1;
                     |    vref := val1;
                     |    p := create Proxy(val1, vref);
                     |  end
                     |end
                     |class Proxy(a,b)
                     |  private
                     |    _3D := ["Box", [a,b,0], [0,0,0], [0.1, 0.2, 0.3], [0, 0, 0]];
                     |  end
                     |end""".stripMargin
    val ast = Parser.run(Parser.prog, progText)
    val classes = ast.defs.map(c => c.name.x -> ClassDesc.extractClassDesc(c)).toMap
    val instances = InstanceTree.extractInstances(ast.defs, classes)
    val xxx = instances.filter(_.name == "p").map(_.findTDPositionOriginVars(0)).flatten
    xxx.map(println)
    xxx(0).gv = GDouble(2.0)
    xxx.map(println)
  }

  def testLocalAggregate2() {
    val progText = """class Main(simulator)
                     |  private
                     |    vec := [1,2,3];
                     |    vecref := vec;
                     |    p := create Proxy(vecref);
                     |  end
                     |end
                     |class Proxy(v)
                     |  private
                     |    _3D := ["Box", v, [1,1,1], [0.1, 0.2, 0.3], [0, 0, 0]];
                     |  end
                     |end""".stripMargin
    val ast = Parser.run(Parser.prog, progText)
    val classes = ast.defs.map(c => c.name.x -> ClassDesc.extractClassDesc(c)).toMap
    val instances = InstanceTree.extractInstances(ast.defs, classes)
    val xxx = instances.filter(_.name == "p").map(_.findTDPositionOriginVars(0)).flatten
    val yyy = instances.filter(_.name == "p").map(_.findTDSizeVars(0)).flatten
//    println(xxx)
    println(yyy)
  }

  def testAdditivePosition() {
    val progText = """class Main(simulator)
                     |  private
                     |    pos := [1,2,3];
                     |    t := create Triforce(pos);
                     |  end
                     |end
                     |class Triforce(pos)
                     |  private
                     |    _3D := ["Box", pos + [-1,-2,-3], [1,1,1], [0.1, 0.2, 0.3], [0, 0, 0]];
                     |  end
                     |end""".stripMargin
    val ast = Parser.run(Parser.prog, progText)
    val classes = ast.defs.map(c => c.name.x -> ClassDesc.extractClassDesc(c)).toMap
    val instances = InstanceTree.extractInstances(ast.defs, classes)
    val ttt = instances.filter(_.name == "t").map(_.findTDPositionOriginVars(0)).flatten
    println(ttt)
  }

  def testImplied() {
    val progText = """class Main(simulator)
                     |  private
                     |    v2 := [0,-4,0];
                     |    t1 := create Table([0,4,0]);
                     |    t2 := create Table(v2)
                     |  end
                     |end
                     |
                     |class Table(pos)
                     |  private
                     |    _3D := [["Box", pos + [2, 0, 2], [1, 1, 1], [0.1, 0.2, 0.3], [0, 0, 0]],
                     |            ["Box", pos + [-2, 0, 2], [1, 1, 1], [0.2, 0.3, 0.4], [0, 0, 0]],
                     |            ["Box", pos + [2, 0, -2], [1, 1, 1], [0.3, 0.4, 0.5], [0, 0, 0]],
                     |            ["Box", pos + [-2, 0, -2], [1, 1, 1], [0.4, 0.5, 0.6], [0, 0, 0]]]
                     |  end
                     |end
                     |""".stripMargin
    val ast = Parser.run(Parser.prog, progText)
    val classes = ast.defs.map(c => c.name.x -> ClassDesc.extractClassDesc(c)).toMap
    val instances = InstanceTree.extractInstances(ast.defs, classes)
    val t1 = instances.filter(_.name == "t1").map(_.findTDPositionOriginVars(0)).flatten
    val t2 = instances.filter(_.name == "t2").map(_.findTDPositionOriginVars(0)).flatten
    println(t1)
    println(t2)
  }
}
