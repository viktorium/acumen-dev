
// Two Tanks Overconstrained Helper Variable Example
//
// Authors:  Michal Konecny and Jan Duracz
// Note:     Run using Semantics -> Enclosure

class Main(simulator) 
  private 
    x1 := 1; x1' := 2;  
    x2 := 1; x2' := -3;
    x12 := 2; x12' := -1; // Helper variable added!
    mode := "Fill1" 
  end
  switch mode
    case "Fill1" 
      claim x1  >= 0 && 
            x2  >= 0 && 
            x12 >= 0 && 
            x12 == x1 + x2 // Constraint added
      if x2 == 0 mode := "Fill2" end; 
      x1'  = 2; 
      x2'  = -3;
      x12' = -1;
    case "Fill2" 
      claim x1 >= 0 && 
            x2 >= 0 && 
            x12 >= 0 && 
            x12 == x1 + x2 // Constraint added
      if x1 == 0 mode := "Fill1" end; 
      x1'  = -2; 
      x2'  = 1;
      x12' = -1; // x12' = x1' + x2' 
  end;
  simulator.endTime := 2.5;
  simulator.minSolverStep := 2.5;
  simulator.minLocalizationStep := 0.00001;
  simulator.minComputationImprovement := 0;
end

// Note:  An additional constraint, based on a helper
//        variable, stabilizes the post-Zeno 
//        enclosure.
//
// Note:  The dynamics and constraints added based on 
//        the helper variable x12 express that the 
//        sum of x1 and x2 is decreasing.
